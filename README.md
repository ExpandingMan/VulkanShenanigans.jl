# VulkanShenanigans

This repository contains some of my exploits learning about Vulkan and graphics rendering in Julia.

The code here is a mess and should not be considered a working package.

The main useful result contained herein, as of writing, is `CuDisplay` and `ExportableCuArray`.
This is a proof of concept implementation for (relatively) efficiently copying data from a CUDA
memory buffer to a Vulkan buffer.  The main application of this as I see it is making it possible to
write the results of CUDA calculations directly (or as directly as possible) into the framebuffer.
In principle, it should be possible to create a rendering engine that operates mainly with CUDA as
opposed to Vulkan.  Whether it's remotely a good idea to do such a thing is dubious.
