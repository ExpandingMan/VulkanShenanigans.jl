import Pkg;  Pkg.activate(joinpath(@__DIR__,"vulkanenv"))
ENV["JULIA_DEBUG"] = "VulkanShenanigans"

using VulkanShenanigans; const V = VulkanShenanigans
using StaticArrays, GLFW, Vulkan, Vulkan.VkCore
using Colors, FileIO, KittyTerminalImages, ImageShow, Infiltrator
using CUDA

using VulkanShenanigans: Engine, Vertex, Affine, Device, Image
using VulkanShenanigans: record_render_command!, main, std140

using VulkanShenanigans: VkArray, VkVector, VkMatrix, vkglobal
using VulkanShenanigans: BufferArray, BufferVector, BufferMatrix
using VulkanShenanigans: Image
using VulkanShenanigans: CuDisplay, run!

const RGBA8 = RGBA{Colors.FixedPointNumbers.N0f8}

const dev = vkglobal(Device)


function cuda_test_matrix()
    sz = (128, 128)
    x1 = fill(BGRA(1,0,0), sz)
    x2 = fill(BGRA(0,1,0), sz)
    x3 = fill(BGRA(0,0,1), sz)
    x4 = fill(BGRA(0,1,1), sz)

    x = [x1 x2
         x3 x4]

    cu(x)
end


src() = quote
    x = cuda_test_matrix()

    cud = CuDisplay(x)

    #run!(cud)
end

src2() = quote
    x = V.cuda_test_matrix()
end

