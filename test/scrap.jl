import Pkg;  Pkg.activate(joinpath(@__DIR__,"vulkanenv"))
ENV["JULIA_DEBUG"] = "VulkanShenanigans"

using VulkanShenanigans; const V = VulkanShenanigans
using StaticArrays, GLFW, Vulkan, Vulkan.VkCore
using Colors, FileIO, KittyTerminalImages, ImageShow

using VulkanShenanigans: Engine, Vertex, Affine, Device, Image
using VulkanShenanigans: record_render_command!, main, std140


scrap() = quote
    e = Engine(undef)

    V.init!(e, 256, 256)
end

serwtf() = quote
    aff = Affine(Float32[1 0 0; 0 1 0; 0 0 1], Float32[0.25, 0, 0])
end

testrun(e::Engine) = main(V.framecall, e)


imagetest() = quote
    sz = (256,256)
    x1 = fill(RGB(1,0,0), sz)
    x2 = fill(RGB(0,1,0), sz)
    x3 = fill(RGB(0,0,1), sz)
    x4 = fill(RGB(0,1,1), sz)

    x = [x1 x2
         x3 x4]
end


scrap2() = quote
    d = Device()
end


#eval(scrap())
#testrun(e)
