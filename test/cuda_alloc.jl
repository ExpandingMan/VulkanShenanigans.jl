#====================================================================================================
This is a proof of concept for allocating a CUmemGenericAllocationHandle and using cuMemMap
to wrap it in a CuArray

NOTE:
The below seems to work, however I have yet to test that it can be properly imported into Vulkan.
Julia file stuff that works on "normal" FD's largely does not seem to work, but I don't know
if that should be surprising.
====================================================================================================#
using CUDA, GPUArrays
using CUDA.Mem

function _mem_allocation_prop(location=CUDA.CU_MEM_LOCATION_TYPE_DEVICE,
                              handle_type=CUDA.CU_MEM_HANDLE_TYPE_POSIX_FILE_DESCRIPTOR,
                              type=CUDA.CU_MEM_ALLOCATION_TYPE_PINNED,
                             )
    loc = CUDA.CUmemLocation(location, 0)
    r = Ref(CUDA.CUmemAllocationProp(ntuple(x -> 0x00, 32)))
    ptr = Base.unsafe_convert(Ptr{CUDA.CUmemAllocationProp}, r)
    ptr.type = type
    ptr.requestedHandleTypes = handle_type
    ptr.location = loc
    GC.@preserve r unsafe_load(ptr)
end

# this is about 256 kb, so surprisingly big
function _getgranularity()
    o = Ref{UInt64}()
    CUDA.cuMemGetAllocationGranularity(o, Ref(_mem_allocation_prop()),
                                       CUDA.CU_MEM_ALLOC_GRANULARITY_MINIMUM,
                                      )
    o[]
end

function _memcreate(n::Integer, sz::Integer=n*_getgranularity())
    h = Ref{CUDA.CUmemGenericAllocationHandle}()
    props = _mem_allocation_prop()
    CUDA.cuMemCreate(h, sz, Ref(props), 0x00)
    h[]
end

function _memmap(ptr::CUDA.CUdeviceptr, handle::CUDA.CUmemGenericAllocationHandle, 
                 n::Integer=1,
                 sz::Integer=n*_getgranularity(),
                )
    CUDA.cuMemMap(ptr, sz, 0, handle, 0x00)
end


#WARN: deliberately leak memory until we decide how to free the d_ptr
function _free_buffer(buf::CUDA.Mem.DeviceBuffer, early)
end

function _allocmemory(n::Integer)
    o = Ref{CUDA.CUdeviceptr}()
    sz = n*_getgranularity()
    CUDA.cuMemAddressReserve(o, sz, 0x00, CuPtr{Nothing}(UInt64(0)), 0x00)
    acc = CUDA.CUmemAccessDesc(CUDA.CUmemLocation(CUDA.CU_MEM_LOCATION_TYPE_DEVICE, 0),
                               CUDA.CU_MEM_ACCESS_FLAGS_PROT_READWRITE,
                              )
    h = _memcreate(n, sz)

    #ex = Ref{Ptr{Nothing}}()
    #CUDA.cuMemExportToShareableHandle(ex, h, CUDA.CU_MEM_HANDLE_TYPE_POSIX_FILE_DESCRIPTOR, 0x00)

    _memmap(o[], h)
    CUDA.cuMemSetAccess(o[], sz, Ref(acc), 1)
    buf = Mem.DeviceBuffer(CUDA.context(), o[], sz, true)
    (buf, h)
end

# later will implement abstract array
struct ExportableCuArray{T,N}
    array::CuArray{T,N,Mem.DeviceBuffer}
    handle::CUDA.CUmemGenericAllocationHandle
end

function ExportableCuArray{T,N}(::typeof(undef), dims;
                                maxsize::Integer=cld(prod(dims)*sizeof(T), _getgranularity()),
                               ) where {T,N}
    (buf, h) = _allocmemory(maxsize)
    ref = DataRef(_free_buffer, buf)
    a = CuArray{T,N}(ref, dims; maxsize)
    ExportableCuArray{T,N}(a, h)
end

#NOTE: `open` acts like it works on this but `read` gives me nothing, which is not necessarily
#surprising

function RawFD(a::ExportableCuArray)
    ex = Ref{Ptr{Nothing}}()
    CUDA.cuMemExportToShareableHandle(ex, a.handle, CUDA.CU_MEM_HANDLE_TYPE_POSIX_FILE_DESCRIPTOR, 0x00)
    RawFD(ex[])
end

src() = quote
    a = ExportableCuArray{Float32,1}(undef, (256,))
end
