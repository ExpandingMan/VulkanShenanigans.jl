using VulkanShenanigans
using Documenter

DocMeta.setdocmeta!(VulkanShenanigans, :DocTestSetup, :(using VulkanShenanigans); recursive=true)

makedocs(;
    modules=[VulkanShenanigans],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/VulkanShenanigans.jl/blob/{commit}{path}#{line}",
    sitename="VulkanShenanigans.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/VulkanShenanigans.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
