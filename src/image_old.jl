
const DEFAULT_IMAGE_USAGE = IMAGE_USAGE_TRANSFER_DST_BIT | IMAGE_USAGE_SAMPLED_BIT


#FUCK: you have to deal with all of this a lot, so you have to make everything in iamges and buffers
# super easy and self contained!!!


struct Image
    image::Vulkan.Image  # contains reference to device

    # again must ensure this only gets freed when image does
    # we don't alllocate it in cases where we get image from somewhere like swapchain
    memory::Union{Nothing,DeviceMemory}

    extent::Extent3D
    format::Format

    #TODO: eventually will likely need mip levels and stuff
end

Vulkan.Image(img::Image) = img.image
Vulkan.DeviceMemory(img::Image) = img.memory
Vulkan.Device(img::Image) = img.image.device

extent(img::Image) = img.extent

function _image_memory(dev::Vulkan.Device, img::Vulkan.Image, mprops::MemoryPropertyFlag)
    reqs = get_image_memory_requirements(dev, img)
    m = allocate_memory(dev, reqs.size, findmemtype(dev, reqs.memory_type_bits, mprops)) |> unwrap
    bind_image_memory(dev, img, m, 0) |> unwrap
    m
end

function Image(dev::Vulkan.Device, ext::Extent3D, format::Format=FORMAT_R8G8B8A8_SRGB;
               mip_levels::Integer=1,
               array_layers::Integer=1,
               usage::ImageUsageFlag=DEFAULT_IMAGE_USAGE,
               samples::SampleCountFlag=SAMPLE_COUNT_1_BIT,
               type::ImageType=IMAGE_TYPE_2D,
               tiling::ImageTiling=IMAGE_TILING_OPTIMAL,
               sharing_mode::SharingMode=SHARING_MODE_EXCLUSIVE,
               layout::ImageLayout=IMAGE_LAYOUT_UNDEFINED,
               queue_family_indices::AbstractVector{<:Integer}=Int[],
               memory_properties::MemoryPropertyFlag=MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
              )
    img = Vulkan.Image(dev, type, format, ext, mip_levels, array_layers, samples, tiling,
                       usage, sharing_mode,
                       queue_family_indices, layout,
                      )
    m = _image_memory(dev, img, memory_properties)
    Image(img, m, ext, format)
end
function Image(dev::Vulkan.Device, ext::Extent2D, format::Format=FORMAT_R8G8B8A8_SRGB; kw...)
    Image(dev, Extent3D(ext.width, ext.height, 1), format; kw...)
end
function Image(dev::Vulkan.Device, (Δx, Δy), format::Format=FORMAT_R8G8B8A8_SRGB; kw...)
    Image(dev, Extent2D(Δx, Δy), format; kw...)
end
Image(dev::Device, a...; kw...) = Image(dev.device, a...; kw...)

# color types are now handled by extension
Vulkan.Format(x::AbstractMatrix) = Vulkan.Format(eltype(x))

default_image_subresource_range() = ImageSubresourceRange(IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1)

function transitionlayout(bcmd::CommandBuffer, img::Image, lyt1::ImageLayout, lyt2::ImageLayout;
                          src_queue_family_index::Integer=QUEUE_FAMILY_IGNORED,
                          dst_queue_family_index::Integer=QUEUE_FAMILY_IGNORED,
                          subresource_range::ImageSubresourceRange=default_image_subresource_range(),
                         )
    if lyt1 == IMAGE_LAYOUT_UNDEFINED && lyt2 == IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        srcmask = ACCESS_NONE
        dstmask = ACCESS_TRANSFER_WRITE_BIT
        srcstage = PIPELINE_STAGE_TOP_OF_PIPE_BIT
        dststage = PIPELINE_STAGE_TRANSFER_BIT
    elseif lyt1 == IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && lyt2 == IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        srcmask = ACCESS_TRANSFER_WRITE_BIT
        dstmask = ACCESS_SHADER_READ_BIT
        srcstage = PIPELINE_STAGE_TRANSFER_BIT
        dststage = PIPELINE_STAGE_FRAGMENT_SHADER_BIT
    else
        throw(ArgumentError("unsopported layout transition from $lyt1 to $lyt2"))
    end
    bar = ImageMemoryBarrier(srcmask, dstmask, lyt1, lyt2,
                             src_queue_family_index, dst_queue_family_index,
                             img.image, subresource_range,
                            )
    cmd_pipeline_barrier(bcmd, [], [], [bar]; src_stage_mask=srcstage, dst_stage_mask=dststage)
    nothing
end

default_image_subresource_layers() = ImageSubresourceLayers(IMAGE_ASPECT_COLOR_BIT, 0, 0, 1)

#TODO: this is going to need to be made a lot more general but would rather wait to get an idea what
#is needed.  Probably will need to keep more info with the image
function copyto_notransition(bcmd::CommandBuffer, img::Image, b::Buffer;
                             subresource_layers::ImageSubresourceLayers=default_image_subresource_layers(),
                            )
    bcopy = BufferImageCopy(0, 0, 0, subresource_layers, Offset3D(0, 0, 0), extent(img))
    cmd_copy_buffer_to_image(bcmd, b.buffer, img.image, IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, [bcopy])
    nothing
end

function Base.copyto!(img::Image, b::Buffer, cmdp::CommandPool, queue::Queue; kw...)
    commandexe(cmdp, queue; kw...) do bcmd
        transitionlayout(bcmd, img, IMAGE_LAYOUT_UNDEFINED, IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
        copyto_notransition(bcmd, img, b)
        transitionlayout(bcmd, img, IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    end
    img
end

function Base.copyto!(img::Image, x::AbstractMatrix{<:Colorant}, cmdp::CommandPool, queue::Queue; kw...)
    if !(img.extent.width == size(x, 2) && img.extent.height == size(x, 1) && Format(x) == img.format)
        throw(ArgumentError("tried to copy an image matrix to an incompatible image object"))
    end
    b = tobuffer(Vulkan.Device(img), x, BUFFER_USAGE_TRANSFER_SRC_BIT)
    copyto!(img, b, cmdp, queue; kw...)
end

function Image(x::AbstractMatrix{<:Colorant}, cmdp::CommandPool, queue::Queue; kw...)
    img = Image(cmdp.device, (size(x,2), size(x,1)), Format(x); kw...)
    copyto!(img, x, cmdp, queue)
end

function Image(dev::Device, x::AbstractMatrix{<:Colorant}, cmdp::CommandPool=CommandPool(dev),
               queue::Queue=Queue(graphicsqueuefamily(dev));
               kw...
              )
    Image(x, cmdp, queue; kw...)
end

default_component_mapping() = ComponentMapping(ntuple(x -> COMPONENT_SWIZZLE_IDENTITY, 4)...)

# let's try this as a convention for "pirated" constructors
function vkImageView(img::Vulkan.Image, format::Format;
                     component_mapping::ComponentMapping=default_component_mapping(),
                     subresource_range::ImageSubresourceRange=default_image_subresource_range(),
                    )
    ImageView(img.device, img, IMAGE_VIEW_TYPE_2D, format, component_mapping, subresource_range)
end

Vulkan.ImageView(img::Image; kw...) = vkImageView(img.image, img.format; kw...)
