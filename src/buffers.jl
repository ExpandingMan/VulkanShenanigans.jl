
const DEFAULT_BUFFER_MEM_PROPS = MEMORY_PROPERTY_HOST_VISIBLE_BIT ∪ MEMORY_PROPERTY_HOST_COHERENT_BIT
const DEFAULT_BUFFER_USAGE = BUFFER_USAGE_TRANSFER_SRC_BIT ∪ BUFFER_USAGE_TRANSFER_DST_BIT

#TODO: need mutable flag for whether buffer has bound memory

struct Buffer
    device::Device
    buffer::Vulkan.Buffer
    size::Int
    usage::BufferUsageFlag
    sharing_mode::SharingMode
    memory::RefValue{Memory}  # make sure memory is not GC'd if bound
    queue_families::Vector{QueueFamily}
end

Device(b::Buffer) = b.device
Vulkan.Device(b::Buffer) = Vulkan.Device(Device(b))
Vulkan.Buffer(b::Buffer) = b.buffer
Vulkan.BufferUsageFlag(b::Buffer) = b.usage
Vulkan.SharingMode(b::Buffer) = b.sharing_mode

Vulkan.MemoryRequirements(b::Buffer) = get_buffer_memory_requirements(Vulkan.Device(b), b.buffer)

Base.sizeof(b::Buffer) = b.size

function Buffer(dev::Device, ::typeof(undef), sz::Integer, usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE;
                sharing_mode::SharingMode=SHARING_MODE_EXCLUSIVE,
                queue_families::AbstractVector{<:QueueFamily}=QueueFamily[],
               )
    idx = map(vkindex, queue_families)
    vkb = Vulkan.Buffer(Vulkan.Device(dev), sz, usage, sharing_mode, idx)
    Buffer(dev, vkb, sz, usage, sharing_mode, RefValue{Memory}(), queue_families)
end

function Buffer(::typeof(undef), sz::Integer, usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE; kw...)
    Buffer(vkglobal(Device), undef, sz, usage; kw...)
end

function Base.copyto!(ctx::VulkanContext, dst::Buffer, src::Buffer;
                      queue::Queue=gfxqueue(ctx),
                      command_pool::CommandPool=gfxcmdpool(ctx),
                      kw...
                     )
    commandexe(command_pool, queue; kw...) do bcmd
        bcopy = BufferCopy(0, 0, src.size)
        cmd_copy_buffer(bcmd, Vulkan.Buffer(src), Vulkan.Buffer(dst), [bcopy])
    end
    dst
end
Base.copyto!(dst::Buffer, src::Buffer; kw...) = copyto!(vkglobal(), dst, src; kw...)


function MemoryRequest(b::Buffer, props::MemoryPropertyFlag=DEFAULT_BUFFER_MEM_PROPS)
    MemoryRequest(get_buffer_memory_requirements(Vulkan.Device(b), Vulkan.Buffer(b)), props)
end

# for now this doesn't do any kind of checking
function bind!(m::Memory, b::Buffer; offset::Integer=0)
    bind_buffer_memory(Vulkan.Device(b), Vulkan.Buffer(b), Vulkan.DeviceMemory(m), offset) |> unwrap
    b.memory[] = m
    m
end

function Memory(b::Buffer, props::MemoryPropertyFlag=DEFAULT_BUFFER_MEM_PROPS; kw...)
    m = Memory(Device(b), undef, MemoryRequest(b, props))
    bind!(m, b; kw...)
end

function VkArray{T}(b::Buffer, dims::NTuple{N,Integer}=(sizeof(m)÷sizeof(T),);
                    memory_properties::MemoryPropertyFlag=DEFAULT_BUFFER_MEM_PROPS,
                    offset::Integer=0,
                   ) where {T,N}
    m = Memory(b, memory_properties)
    VkArray{T}(m, undef, dims; offset)
end


# only difference from VkArray is that this is guaranteed backed by buffer
struct BufferArray{T,N} <: VkAbstractArray{T,N}
    buffer::Buffer
    array::VkArray{T,N}
end

Buffer(v::BufferArray) = v.buffer
Memory(v::BufferArray) = Memory(v.array)
Device(v::BufferArray) = Device(Memory(v))

innerarray(x::BufferArray) = innerarray(x.array)

const BufferVector{T} = BufferArray{T,1}
const BufferMatrix{T} = BufferArray{T,2}

#TODO: clean up constructors, Vector and Matrix don't work

function BufferArray{T}(dev::Device, ::typeof(undef), dims::NTuple{N,Integer},
                        usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE;
                        memory_properties::MemoryPropertyFlag=DEFAULT_BUFFER_MEM_PROPS,
                        offset::Integer=0,
                        kw...
                       ) where {T,N}
    b = Buffer(dev, undef, sizeof(T)*prod(dims), usage; kw...)
    v = VkArray{T}(b, dims; memory_properties, offset)
    BufferArray{T,N}(b, v)
end
function BufferArray{T}(::typeof(undef), dims::NTuple{N,Integer}, usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE;
                        kw...) where {T,N}
    BufferArray{T}(vkglobal(Device), undef, dims, usage; kw...)
end

function BufferArray{T}(dev::Device, x::AbstractArray, usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE; kw...) where {T}
    x′ = BufferArray{T}(dev, undef, size(x), usage; kw...)
    copyto!(x′, x)
end
function BufferArray(dev::Device, x::AbstractArray, usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE; kw...) 
    BufferArray{eltype(x)}(dev, x, usage; kw...)
end

function BufferArray{T}(x::AbstractArray, usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE; kw...) where {T}
    BufferArray{T}(vkglobal(Device), x, usage; kw...)
end
function BufferArray(x::AbstractArray, usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE; kw...)
    BufferArray{eltype(x)}(x, usage; kw...)
end

Base.setindex!(v::BufferArray, x, j::Int) = setindex!(innerarray(v), x, j)

Base.copyto!(ctx::VulkanContext, dst::BufferArray, src::Buffer; kw...) = copyto!(ctx, dst.buffer, src)
Base.copyto!(dst::BufferArray, src::Buffer; kw...) = copyto!(vkglobal(), dst, src; kw...)


#TODO: this stuff
"""
    stagedtobuffer(dev::Device, x, usage, command_pool, queue,
                   mprops=(MEMORY_PROPERTY_HOST_VISIBLE_BIT | MEMORY_PROPERTY_HOST_COHERENT_BIT);
                   kw...
                  )

Just like [`tobuffer`](@ref) this writes the object `x` to a new buffer but it ensures that this is done from a staging
buffer on the host to a local buffer on the device.
"""
function stagedtobuffer(dev::Vulkan.Device, x, usage::BufferUsageFlag,
                        cmdp::CommandPool, queue::Queue,
                        mprops::MemoryPropertyFlag=DEFAULT_BUFFER_MEM_PROPS;
                        command_buffer_level::CommandBufferLevel=COMMAND_BUFFER_LEVEL_PRIMARY,
                        wait::Bool=true,
                        size::Integer=sizeof(x),
                        kw...
                       )
    b = tobuffer(dev, x, BUFFER_USAGE_TRANSFER_SRC_BIT; size)
    b′ = Buffer(dev, size, BUFFER_USAGE_TRANSFER_DST_BIT | usage, MEMORY_PROPERTY_DEVICE_LOCAL_BIT; kw...)
    copyto!(b′, b, cmdp, queue; command_buffer_level, wait)
end
function stagedtobuffer(dev::Device, x, usage::BufferUsageFlag, cmdp::CommandPool, queue::Queue,
                        mprops::MemoryPropertyFlag=DEFAULT_BUFFER_MEM_PROPS; kw...)
    stagedtobuffer(dev.device, x, usage, cmdp, queue, mprops; kw...)
end
