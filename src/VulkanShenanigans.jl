module VulkanShenanigans

using GLFW, Vulkan, Vulkan.VkCore, ResultTypes
using Transducers, StaticArrays, Rotations
using Colors, ColorTypes, FileIO, FixedPointNumbers
using glslang_jll

using Transducers: Filter

using Base: RefValue
using Base.Threads: Atomic

using Infiltrator


const glslang = glslangValidator()

# store this globally for now for safety
const SWAPCHAIN_IMAGE_FORMAT = FORMAT_B8G8R8A8_UNORM


include("bitmask_set_ops.jl")
include("glfw.jl")
include("device.jl")


struct VulkanContext
    device::Device

    cmdpool_gfx::CommandPool
end


include("array.jl")
include("buffers.jl")
include("surface.jl")
include("image.jl")


VulkanContext(d::Device=Device()) = VulkanContext(d, CommandPool(d))

Device(ctx::VulkanContext) = ctx.device

Vulkan.Device(ctx::VulkanContext) = Vulkan.Device(Device(ctx))

gfxcmdpool(ctx::VulkanContext) = ctx.cmdpool_gfx

gfxqueue(ctx::VulkanContext, idx::Integer=1) = gfxqueue(Device(ctx))

const GLOBAL_CONTEXT = RefValue{VulkanContext}()

function vkglobal!(ctx::VulkanContext=VulkanContext())
    GLOBAL_CONTEXT[] = ctx
    GC.gc()  # we GC here in case there was an old context
    ctx
end

vkglobal(::Type{VulkanContext}) = GLOBAL_CONTEXT[]
vkglobal(::Type{Device}) = Device(vkglobal(VulkanContext))
vkglobal() = vkglobal(VulkanContext)


__init__() = vkglobal!()


"""
    worldtime()

Get the value of a global timer in floating point seconds.
"""
worldtime() = float(time_ns()) * 10^(-9)

#TODO: this is very temporary
struct Vertex
    pos::SVector{3,Float32}
    col::SVector{3,Float32}
    texture::SVector{2,Float32}
end

mutable struct Engine
    context::VulkanContext

    window::Window
    surface::Surface
    device::Device

    # synchronization; each of these get 1 per frame
    image_available::Vector{Semaphore}
    render_finished::Vector{Semaphore}
    in_flight::Vector{Fence}

    render_pass::RenderPass
    pipelines::Vector{Pipeline}
    pipeline_layout::PipelineLayout
    swapchain::Union{Nothing,SwapchainKHR}  # can get deleted so we need a null option
    image_views::Vector{ImageView}
    framebuffers::Vector{Framebuffer}

    descriptor_set_layout::DescriptorSetLayout
    descriptor_pool::DescriptorPool
    descriptor_sets::Vector{DescriptorSet}

    texture_image::Image  # we need this here too or else it will be GC'd
    texture_image_view::ImageView
    texture_sampler::Sampler

    vtx_buffer::Buffer
    idx_buffer::Buffer

    uniform_buffers::Vector{Buffer}  # 1 per frame

    # these are explicitly freed in deinit!
    command_buffers::Vector{CommandBuffer}  # 1 per frame (I think)

    # configuration options
    config_timeout_draw::Int
    max_frames_in_flight::Int

    frame_idx::Int
    frame_resized::Bool

    # timing (s)
    t0::Float64

    # references to stuff we want not to get GC'd
    refs::AbstractVector{Ref}

    function Engine(::typeof(undef);
                    timeout_draw::Integer=typemax(Int),
                    max_frames_in_flight::Integer=2,
                   )
        e = new()

        for ϕ ∈ fieldnames(Engine)
            T = fieldtype(Engine, ϕ)
            T >: Nothing && setfield!(e, ϕ, nothing)
            T <: AbstractVector && setfield!(e, ϕ, Vector{eltype(T)}())
        end

        e.config_timeout_draw = timeout_draw
        e.max_frames_in_flight = max_frames_in_flight
        e.frame_idx = 1
        e.frame_resized = false
        e.refs = Vector{Ref}()
        e
    end
end

#====================================================================================================
       Wrappers to create:

- input bindings descriptions for for arbitrary Julia types
====================================================================================================#

#====================================================================================================
       TODO:

- Something fishy is still happening when you try to smoothly resize the window.
    Suspect it involves getting multiple resizes before the swapchain can even be re-created
    but I'm only getting validation errors (couldn't find Swapchain) so this seems hard to fix.
    Probably need a more sophisticated resize routine like force drawing a frame every time
    you resize.
====================================================================================================#


include("glsl.jl")
include("transforms.jl")
include("tutorial.jl")


include("cuda.jl")


end
