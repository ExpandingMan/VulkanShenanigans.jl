
# obviously defaults may need to be expanded at some point...
const DEFAULT_VK_INSTANCE_EXTENSIONS = ["VK_EXT_debug_utils", "VK_KHR_surface", "VK_KHR_xcb_surface"]
const DEFAULT_VK_DEVICE_LAYERS = String[]
const DEFAULT_VK_DEVICE_EXTENSIONS = ["VK_KHR_swapchain"]
const DEBUG_MESSAGE_SEVERITY = |(DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT,
                                 DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,
                                 DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,
                                 DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
                                )
const DEBUG_MESSAGE_TYPES = |(DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
                              DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,
                              DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
                             )



struct Device
    device::Vulkan.Device

    # memory type index is array index - 1
    memory_properties::Vector{MemoryPropertyFlag}

    qfam_indices::Dict{QueueFlag,Vector{Int}}
    qfam_properties::Vector{QueueFamilyProperties}  # no way to get these individually
end

# note that on RTX 4090 the first queue just does everything, so we don't wind up dealing with this

struct QueueFamily
    index::Int  # vulkan index, 0-based
    properties::QueueFamilyProperties
    device::Device
end

function Device(dev::Vulkan.Device) 
    mprops = get_physical_device_memory_properties(dev.physical_device).memory_types |>
        Map(t -> t.property_flags) |> collect
    Device(dev, mprops, Dict{QueueFlag,Vector{Int}}(), Vector{QueueFamilyProperties}())
end

Device(qf::QueueFamily) = qf.device

Vulkan.Device(dev::Device) = dev.device
Vulkan.PhysicalDevice(dev::Device) = Vulkan.Device(dev).physical_device
Vulkan.Instance(dev::Device) = PhysicalDevice(dev).instance

Vulkan.Semaphore(dev::Device; kw...) = Semaphore(dev.device; kw...)
Vulkan.Fence(dev::Device; kw...) = Fence(dev.device; kw...)

Vulkan.MemoryPropertyFlag(dev::Device, j::Integer) = dev.memory_properties[j+1]

function update_queue_families!(dev::Device, flags::AbstractVector{QueueFlag}=values(QueueFlag))
    empty!(dev.qfam_indices)
    empty!(dev.qfam_properties)
    for f ∈ flags
        dev.qfam_indices[f] = Int[]
    end
    props = get_physical_device_queue_family_properties(Vulkan.PhysicalDevice(dev))
    append!(dev.qfam_properties, props)
    for (i, prop) ∈ enumerate(props)
        dev.qfam_properties[i] = prop
        for f ∈ flags
            (f & prop.queue_flags) > 0 && push!(dev.qfam_indices[f], i)
        end
    end
    dev
end

function all_queue_infos(pdev::PhysicalDevice)
    qfis = get_physical_device_queue_family_properties(pdev)
    #TODO: need a way to set the queue priorities
    map(idx -> DeviceQueueCreateInfo(idx, [1.0f0]), 0:(length(qfis)-1))
end

#TODO: this has to be handled in a less retarded way
default_physical_device_features() = PhysicalDeviceFeatures(:sampler_anisotropy)

function Device(pdev::PhysicalDevice;
                device_layers::AbstractVector{<:AbstractString}=DEFAULT_VK_DEVICE_LAYERS,
                device_extensions::AbstractVector{<:AbstractString}=DEFAULT_VK_DEVICE_EXTENSIONS,
                enabled_features::PhysicalDeviceFeatures=default_physical_device_features(),
                init_queue_families::Bool=true,
               )
    qs = all_queue_infos(pdev)
    vkdev = Vulkan.Device(pdev, qs, device_layers, device_extensions; enabled_features)
    dev = Device(vkdev)
    init_queue_families && update_queue_families!(dev)
    dev
end

function findfirstmemtype(𝒻, dev::Device)
    ps = dev.memory_properties
    for j ∈ 0:(length(ps)-1)
        𝒻(j, ps[j+1]) && return j
    end
    error("no compatible memory type found")
end

function findfirstmemtype(dev::Device, props::MemoryPropertyFlag, types)
    findfirstmemtype(dev) do j, p
        (j ∈ types) && (props ⊆ p)
    end
end
findfirstmemtype(props::MemoryPropertyFlag, types) = findfirstmemtype(vkglobal(Device), props, types)

findfirstmemtype(dev::Device, props::MemoryPropertyFlag, ::Nothing=nothing)  = findfirstmemtype((_, p) -> props ⊆ p, dev)
findfirstmemtype(props::MemoryPropertyFlag, ::Nothing=nothing) = findfirstmemtype(vkglobal(Device), props)

nqueuefamilies(dev::Device) = length(dev.qfam_properties)

function QueueFamily(dev::Device, j::Integer)
    n = nqueuefamilies(dev)
    j > n && throw(ArgumentError("invalid queue family index $j ($n available"))
    QueueFamily(j-1, dev.qfam_properties[j], dev)
end

vkindex(qf::QueueFamily) = qf.index

Vulkan.QueueFlag(qf::QueueFamily) = qf.properties.queue_flags

Vulkan.Device(qf::QueueFamily) = qf |> Device |> Vulkan.Device

hasflag(qf::QueueFamily, f::QueueFlag) = (QueueFlag(qf) & f) > 0

queuefamilies(dev::Device) = map(j -> QueueFamily(dev, j), 1:nqueuefamilies(dev))

queuefamilies(dev::Device, flags::QueueFlag) = filter!(f -> hasflag(qf, f), queuefamilies(dev))

firstqueuefamily(dev::Device, flags::QueueFlag) = QueueFamily(dev, first(dev.qfam_indices[flags]))

isgraphics(qf::QueueFamily) = QueueFlag(qf) & QUEUE_GRAPHICS_BIT > 0

function defaultmessenger(severity::DebugUtilsMessageSeverityFlagEXT=DEBUG_MESSAGE_SEVERITY,
                          types::DebugUtilsMessageTypeFlagEXT=DEBUG_MESSAGE_TYPES,
                         )
    cb = @cfunction(default_debug_callback, UInt32,
                    (DebugUtilsMessageSeverityFlagEXT, DebugUtilsMessageTypeFlagEXT,
                     Ptr{VkCore.VkDebugUtilsMessengerCallbackDataEXT}, Ptr{Cvoid}),
                   )
    DebugUtilsMessengerCreateInfoEXT(severity, types, cb)
end

function _default_app_info(appver::VersionNumber, engver::VersionNumber, apiver::VersionNumber,
                           appname::AbstractString, engname::AbstractString)
    ApplicationInfo(appver, engver, apiver; application_name=appname, engine_name=engname)
end

function instance(exts::AbstractVector{<:AbstractString}=DEFAULT_VK_INSTANCE_EXTENSIONS;
                  # for layers
                  validate::Bool=true,
                  layers::AbstractVector{<:AbstractString}=(validate ? ["VK_LAYER_KHRONOS_validation"] : String[]),
                  # for app info
                  app_version::VersionNumber=v"0.1.0",
                  engine_version::VersionNumber=v"0.1.0",
                  api_version::VersionNumber=v"1.3",
                  application_name::AbstractString="",
                  engine_name::AbstractString="",
                  app_info::ApplicationInfo=_default_app_info(app_version, engine_version, api_version, application_name,
                                                              engine_name),
                  # messenger
                  messenger::DebugUtilsMessengerCreateInfoEXT=defaultmessenger(),
                 )
    Instance(layers, exts; application_info=app_info, next=messenger)
end

function Device(vki::Instance, devnum::Integer=1; kw...)
    pdev = unwrap(enumerate_physical_devices(vki))[1]
    Device(pdev; kw...)
end

function Device(devnum::Integer=1, exts::AbstractVector{<:AbstractString}=DEFAULT_VK_INSTANCE_EXTENSIONS;
                device_layers::AbstractVector{<:AbstractString}=DEFAULT_VK_DEVICE_LAYERS,
                device_extensions::AbstractVector{<:AbstractString}=DEFAULT_VK_DEVICE_EXTENSIONS,
                kw...
               )
    vki = instance(exts; kw...)
    Device(vki, devnum; device_layers, device_extensions)
end

Vulkan.Queue(qf::QueueFamily, idx::Integer=1) = get_device_queue(Vulkan.Device(qf), qf.index, idx-1)

gfxqueuefamilies(dev::Device) = queuefamilies(dev, QUEUE_GRAPHICS_BIT)

gfxqueuefamily(dev::Device) = firstqueuefamily(dev, QUEUE_GRAPHICS_BIT)

gfxqueue(dev::Device, idx::Integer=1) = Queue(gfxqueuefamily(dev), idx)

Base.wait(dev::Device) = device_wait_idle(dev.device) |> unwrap

function Base.wait(dev::Device, fences::AbstractVector{Fence}; all::Bool=true, timeout::Integer=typemax(Int))
    wait_for_fences(dev.device, fences, all, UInt(timeout)) |> unwrap
end
Base.wait(dev::Device, fence::Fence; kw...) = wait(dev, [fence]; kw...)

Base.reset(dev::Device, fences::AbstractVector{Fence}) = reset_fences(dev.device, fences) |> unwrap
Base.reset(dev::Device, fence::Fence) = reset(dev, [fence])

function Vulkan.CommandPool(dev::Device, qf::QueueFamily=gfxqueuefamily(dev);
                            flags::CommandPoolCreateFlag=COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
                           )
    CommandPool(dev.device, qf.index; flags)
end

"""
    alloc_cmd_buffers(cmdp::CommandPool, n=1; kw...)

Allocate `n` command buffers in the command pool `cmdp`.
"""
function alloc_cmd_buffers(cmdp::CommandPool, n::Integer=1;
                           command_buffer_level::CommandBufferLevel=COMMAND_BUFFER_LEVEL_PRIMARY,
                          )
    info = CommandBufferAllocateInfo(cmdp, command_buffer_level, n)
    allocate_command_buffers(cmdp.device, info) |> unwrap
end

"""
    command(𝒻, cmdp::CommandPool; kw...)

Create a single command buffer in the command pool `cmdp`.  The command will be recorded by executing
`𝒻(bcmd)` where `bcmd` is a newly allocated command buffer.
"""
function command(𝒻, cmdp::CommandPool;
                 command_buffer_level::CommandBufferLevel=COMMAND_BUFFER_LEVEL_PRIMARY,
                )
    bcmd = first(alloc_cmd_buffers(cmdp; command_buffer_level))
    info = CommandBufferBeginInfo(flags=COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT)
    begin_command_buffer(bcmd, info) |> unwrap
    𝒻(bcmd)
    end_command_buffer(bcmd) |> unwrap
    bcmd
end

"""
    commandexe(bcmd, queue::Queue; wait=true, free=true)

Execute the commands in command buffers `bcmd` on `queue`.  If `wait` will wait for queue to be idle before
returning.  If `free && wait` will free the command buffers on completion
"""
function commandexe(bcmd::AbstractVector{CommandBuffer}, queue::Queue;
                    wait::Bool=true, free::Bool=true
                   )
    isempty(bcmd) && return nothing
    info = SubmitInfo([], [], bcmd, [])
    queue_submit(queue, [info]) |> unwrap
    if wait
        queue_wait_idle(queue) |> unwrap
        cmdp = first(bcmd).command_pool
        free && free_command_buffers(cmdp.device, cmdp, bcmd)
    end
    bcmd
end
commandexe(bcmd::CommandBuffer, queue::Queue; kw...) = commandexe([bcmd], queue; kw...)

"""
    commandexe(𝒻, cmdp::CommandPool, queue::Queue; wait=true, free=true, kw...)

Create a command buffer `bcmd` and record a command onto it with `𝒻(bcmd)`, then execute on `queue`.
If `wait` will wait for queue to be idle before returning.  If `free && wait` will free the allocated buffers.
"""
function commandexe(𝒻, cmdp::CommandPool, queue::Queue;
                    wait::Bool=true, free::Bool=true,
                    kw...
                   )
    bcmd = command(𝒻, cmdp; kw...)
    commandexe(bcmd, queue; wait, free)
end
