
function glslcompile(code::AbstractString, type::AbstractString;
                     flags::AbstractVector{<:AbstractString}=String[],
                     verbose::Bool=false,
                    )
    io = IOBuffer()
    write(io, code); seekstart(io)
    verbose && @info("compiling SPIR-V:\n\n$code")
    flags = [flags; "--stdin"; "-V"; "-S"; type]
    path = tempname()
    out = IOBuffer()
    try
        run(pipeline(`$glslang $flags -o $path`; stdin=io, stdout=out))
    catch e
        @error("error in GLSL compilation:\n\n"*String(take!(out)))
        rethrow(e)
    end
    spirv = reinterpret(UInt32, read(path))
    isfile(path) && rm(path)
    (first(spirv) == 0x07230203) || error("SPIR-V code did not compile correctly")
    spirv
end

function glslcompilefile(fname::AbstractString,
                         type::AbstractString=last(split(fname, "."));  # infer from file name by default
                         kw...
                        )
    open(io -> glslcompile(read(io, String), type; kw...), fname)
end
