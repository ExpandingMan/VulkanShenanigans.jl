
struct SurfaceCapabilities
    surface_capabilities::SurfaceCapabilitiesKHR  # this should probably be moved elsewhere
    formats::Vector{SurfaceFormatKHR}
    present_modes::Vector{PresentModeKHR}
end

function SurfaceCapabilities(pdev::PhysicalDevice, srf::Ptr)
    cap = get_physical_device_surface_capabilities_khr(pdev, srf) |> unwrap
    fmts = get_physical_device_surface_formats_khr(pdev; surface=srf) |> unwrap
    pms = get_physical_device_surface_present_modes_khr(pdev; surface=srf) |> unwrap
    SurfaceCapabilities(cap, fmts, pms)
end
SurfaceCapabilities(dev::Device, srf::Ptr) = SurfaceCapabilities(PhysicalDevice(dev), srf)
SurfaceCapabilities(dev::Union{Device,PhysicalDevice}, srf::SurfaceKHR) = SurfaceCapabilities(dev, srf.vks)

function defaultpresentmode(srfc::SurfaceCapabilities)
    if PRESENT_MODE_IMMEDIATE_KHR ∈ srfc.present_modes
        PRESENT_MODE_IMMEDIATE_KHR
    else
        PRESENT_MODE_FIFO_KHR
    end
end

currentextent(srfc::SurfaceCapabilities) = srfc.surface_capabilities.current_extent
minextent(srfc::SurfaceCapabilities) = srfc.surface_capabilities.min_image_extent
maxextent(srfc::SurfaceCapabilities) = srfc.surface_capabilities.max_image_extent
minimagecount(srfc::SurfaceCapabilities) = srfc.surface_capabilities.min_image_count
currenttransform(srfc::SurfaceCapabilities) = srfc.surface_capabilities.current_transform


struct Surface
    window::Window
    device::Device
    surface::SurfaceKHR
    capabilities::RefValue{SurfaceCapabilities}

    present_queue_families::Vector{QueueFamily}
end

Device(srf::Surface) = srf.device
Vulkan.PhysicalDevice(srf::Surface) = srf |> Device |> PhysicalDevice
Window(srf::Surface) = srf.window
Vulkan.SurfaceKHR(srf::Surface) = srf.surface

capabilities(srf::Surface) = SurfaceCapabilities(PhysicalDevice(srf), srf.surface)
capabilities!(srf::Surface) = (srf.capabilities[] = capabilities(srf))

SurfaceCapabilities(srf::Surface) = srf.capabilities[]

for fname ∈ (:currentextent, :minextent, :maxextent, :minimagecount, :currenttransform, :defaultpresentmode)
    @eval $fname(srf::Surface) = $fname(SurfaceCapabilities(srf))
end

# destroy! probably needs to do more here...
for fname ∈ (:shouldclose, :destroy!)
    @eval $fname(srf::Surface) = $fname(Window(srf))
end

currentextent!(srf::Surface) = (capabilities!(srf); currentextent(srf))

#TODO: how to do this with QueueFamily??
function update_present_queue_family_indices!(srf::Surface)
    pdev = PhysicalDevice(srf)
    empty!(srf.present_queue_families)
    for (i, prop) ∈ enumerate(get_physical_device_queue_family_properties(pdev))
        if unwrap(get_physical_device_surface_support_khr(pdev, i-1, SurfaceKHR(srf))) > 0
            push!(srf.present_queue_families, i-1)
        end
    end
    srf.present_queue_families
end

function update_present_queue_families!(srf::Surface)
    empty!(srf.present_queue_families)
    for qf ∈ queuefamilies(Device(srf))
        if unwrap(get_physical_device_surface_support_khr(PhysicalDevice(srf), qf.index, SurfaceKHR(srf))) > 0
            push!(srf.present_queue_families, qf)
        end
    end
    srf.present_queue_families
end

function Surface(dev::Device, w::Window; find_present_queue_families::Bool=true)
    vki = Instance(dev)
    srf = SurfaceKHR(createsurface(vki, w), x -> destroy_surface_khr(x.instance, x), vki)
    srf = Surface(w, dev, srf, Ref(SurfaceCapabilities(dev, srf)), Int[])
    find_present_queue_families && update_present_queue_families!(srf)
    srf
end
Surface(ctx::VulkanContext, w::Window; kw...) = Surface(Device(ctx), w; kw...)

presentqueuefamilies(srf::Surface) = srf.present_queue_families

presentqueuefamily(srf::Surface, i::Integer=1) = presentqueuefamilies(srf)[i]
