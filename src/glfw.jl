
struct Monitor
    monitor::GLFW.Monitor
end

getprimary(::Type{Monitor}) = Monitor(GLFW.GetPrimaryMonitor())

getvidmode(m::Monitor) = VidMode(GLFW.GetVideoMode(m.monitor))


# I define this mostly because I want to be able to define new constructors
struct VidMode
    Δx::Int
    Δy::Int
    rgb::Tuple{Int,Int,Int}
    ν::Int

    function VidMode(Δx::Integer, Δy::Integer,
                     rgb::Union{Nothing,NTuple{3,Integer}}=nothing,
                     ν::Union{Nothing,Integer}=nothing,
                    )
        if isnothing(rgb) || isnothing(ν)
            μ = getprimary(VidMode)
            isnothing(rgb) && (rgb = μ.rgb)
            isnothing(ν) && (ν = μ.ν)
        end
        new(Δx, Δy, rgb, ν)
    end
end

VidMode(m::GLFW.VidMode) = VidMode(m.width, m.height, (m.redbits, m.greenbits, m.bluebits), m.refreshrate)

getprimary(::Type{VidMode}) = getvidmode(getprimary(Monitor))


struct Window
    window::GLFW.Window
end

function windowhints(μ::VidMode=getprimary(VidMode);
                     api::Integer=GLFW.NO_API,
                     resizable::Bool=true,
                     visible::Bool=true,
                     decorated::Bool=false,
                     focused::Bool=true,
                    )
    # unbelievably, window hints are a global state
    GLFW.DefaultWindowHints()
    
    GLFW.WindowHint(GLFW.RED_BITS, μ.rgb[1])
    GLFW.WindowHint(GLFW.GREEN_BITS, μ.rgb[2])
    GLFW.WindowHint(GLFW.BLUE_BITS, μ.rgb[3])
    GLFW.WindowHint(GLFW.REFRESH_RATE, μ.ν)

    GLFW.WindowHint(GLFW.CLIENT_API, api)

    GLFW.WindowHint(GLFW.RESIZABLE, resizable)
    GLFW.WindowHint(GLFW.VISIBLE, visible)
    GLFW.WindowHint(GLFW.DECORATED, decorated)
end

function Window(title::AbstractString, μ::VidMode=getprimary(VidMode), m::Union{Nothing,Monitor}=nothing;
                share::Union{Nothing,Window}=nothing,
                framebuffer_size_callback=nothing,
                kw...
               )
    windowhints(μ; kw...)
    w = Window(if isnothing(share) && isnothing(m)
        GLFW.CreateWindow(μ.Δx, μ.Δy, title)
    elseif isnothing(share)
        GLFW.CreateWindow(μ.Δx, μ.Δy, title, m)
    elseif isnothing(m)
        GLFW.CreateWindow(μ.Δx, μ.Δy, title, getprimary(Monitor).monitor, share)
    else
        GLFW.CreateWindow(μ.Δx, μ.Δy, title, m, share)
    end)
    isnothing(framebuffer_size_callback) || framebuffersize!(framebuffer_size_callback, w)
    w
end
function Window(title::AbstractString, Δx::Integer, Δy::Integer, m::Union{Nothing,Monitor}=nothing;
                rgb::Union{Nothing,NTuple{3,Integer}}=nothing,
                ν::Union{Nothing,Integer}=nothing,
                kw...
               )
    μ = VidMode(Δx, Δy, rgb, ν)
    Window(title, μ, m; kw...)
end

framebuffersize!(𝒻, w::Window) = GLFW.SetFramebufferSizeCallback(w.window, 𝒻)

framebuffersize(w::Window) = GLFW.GetFramebufferSize(w.window)

createsurface(vkinstance::Instance, w::Window) = GLFW.CreateWindowSurface(vkinstance, w.window)

shouldclose(w::Window) = GLFW.WindowShouldClose(w.window)

destroy!(w::Window) = GLFW.DestroyWindow(w.window)

