using BitMasks

#====================================================================================================
All of this is type piracy but I got really sick of not having it...
consider a PR
====================================================================================================#

Base.:(∪)(a::T, b::T) where {T<:BitMask} = a | b
Base.:(∩)(a::T, b::T) where {T<:BitMask} = a & b

Base.:(⊆)(a::T, b::T) where {T<:BitMask} = (a ∩ b) == a
Base.:(⊆)(b::BitMask) = Base.Fix2(⊆, b)

Base.setdiff(a::T, b::T) where {T<:BitMask} = (a ∩ (~b))
