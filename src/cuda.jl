#====================================================================================================
This serves as a PoC for copying CUDA data directly into the framebuffer (well, swapchain).
Note that because you have to do some voodoo to get an exportable CuArray, you can't just swap
in your own CuArray, you have to use the one provided by `ExportableCuArray`, but you should
be free to copy into it.

I can't *really* verify that the host isn't involved at all here, but it seems to be working
correctly.

See
https://developer.nvidia.com/blog/introducing-low-level-gpu-virtual-memory-management/
for more details on some of the memory crap
====================================================================================================#
using CUDA, GPUArrays, CUDA.Mem


function _cuda_mem_allocation_prop(location=CUDA.CU_MEM_LOCATION_TYPE_DEVICE,
                              handle_type=CUDA.CU_MEM_HANDLE_TYPE_POSIX_FILE_DESCRIPTOR,
                              type=CUDA.CU_MEM_ALLOCATION_TYPE_PINNED,
                             )
    loc = CUDA.CUmemLocation(location, 0)
    r = Ref(CUDA.CUmemAllocationProp(ntuple(x -> 0x00, 32)))
    ptr = Base.unsafe_convert(Ptr{CUDA.CUmemAllocationProp}, r)
    ptr.type = type
    ptr.requestedHandleTypes = handle_type
    ptr.location = loc
    GC.@preserve r unsafe_load(ptr)
end

# this is about 256 kb, so surprisingly big
function _cuda_getgranularity()
    o = Ref{UInt64}()
    CUDA.cuMemGetAllocationGranularity(o, Ref(_cuda_mem_allocation_prop()),
                                       CUDA.CU_MEM_ALLOC_GRANULARITY_MINIMUM,
                                      )
    o[]
end

function _cuda_memcreate(n::Integer, sz::Integer=n*_getgranularity())
    h = Ref{CUDA.CUmemGenericAllocationHandle}()
    props = _cuda_mem_allocation_prop()
    CUDA.cuMemCreate(h, sz, Ref(props), 0x00)
    h[]
end

function _cuda_memmap(ptr::CUDA.CUdeviceptr, handle::CUDA.CUmemGenericAllocationHandle, 
                      n::Integer=1,
                      sz::Integer=n*_cuda_getgranularity(),
                     )
    CUDA.cuMemMap(ptr, sz, 0, handle, 0x00)
end

function _cuda_allocmemory(n::Integer)
    (buf, sz, h)
end

# later will implement abstract array
mutable struct ExportableCuArray{T,N}
    array::CuArray{T,N,Mem.DeviceBuffer}
    total_buffer_size::Int
    handle::CUDA.CUmemGenericAllocationHandle

    function ExportableCuArray{T,N}(a::CuArray, sz::Integer, h::CUDA.CUmemGenericAllocationHandle) where {T,N}
        finalizer(new{T,N}(a, sz, h)) do ea
            CUDA.cuMemUnmap(convert(CUDA.CUdeviceptr, pointer(ea.array)), ea.total_buffer_size)
            CUDA.cuMemRelease(ea.handle)
        end
    end
end

function ExportableCuArray{T,N}(::typeof(undef), dims;
                                granularity::Integer=_cuda_getgranularity(),
                                maxsize::Integer=cld(prod(dims)*sizeof(T), granularity),
                               ) where {T,N}
    sz = maxsize*granularity
    o = Ref{CUDA.CUdeviceptr}()
    CUDA.cuMemAddressReserve(o, sz, 0x00, CuPtr{Nothing}(UInt64(0)), 0x00)
    acc = CUDA.CUmemAccessDesc(CUDA.CUmemLocation(CUDA.CU_MEM_LOCATION_TYPE_DEVICE, 0),
                               CUDA.CU_MEM_ACCESS_FLAGS_PROT_READWRITE,
                              )
    h = _cuda_memcreate(maxsize, sz)
    _cuda_memmap(o[], h)
    CUDA.cuMemSetAccess(o[], sz, Ref(acc), 1)
    buf = Mem.DeviceBuffer(CUDA.context(), o[], sz, true)
    ref = DataRef(CUDA._free_buffer, buf)
    a = CuArray{T,N}(ref, dims; maxsize)
    ExportableCuArray{T,N}(a, sz, h)
end

function ExportableCuArray(x::AbstractArray{T,N}; kw...) where {T,N}
    o = ExportableCuArray{T,N}(undef, size(x); kw...)
    copyto!(o.array, x)
    o
end

Base.sizeof(a::ExportableCuArray) = sizeof(a.array)

function _exportablefd(a::ExportableCuArray)
    #WARN: this looks scary, it says it's a void* but it looks like it only writes 32 bits
    ex = Ref{UInt32}(0x00)
    CUDA.cuMemExportToShareableHandle(ex, a.handle, CUDA.CU_MEM_HANDLE_TYPE_POSIX_FILE_DESCRIPTOR, 0x00)
    ex[]
end

_cuda_mem_handle_type() = EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT

function _device_memory_from_cuda(dev::Device, h, sz::Integer)
    info = ImportMemoryFdInfoKHR(h, handle_type=_cuda_mem_handle_type())
    # not at all sure how to set this...
    # seems like a reasonable guess but also seems like it should be its own type
    typeid = findfirstmemtype(dev, MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
    m = allocate_memory(dev.device, sz, typeid; next=info) |> unwrap
    Memory(dev, m, sz, typeid, Vulkan.MemoryPropertyFlag(dev, typeid))
end

Memory(dev::Device, v::ExportableCuArray) = _device_memory_from_cuda(dev, _exportablefd(v), sizeof(v))
Memory(v::ExportableCuArray) = Memory(vkglobal(Device), v)

function cuda_buffer(m::Memory)
    info = ExternalMemoryBufferCreateInfo(handle_types=_cuda_mem_handle_type())
    sz = sizeof(m)
    usage = BUFFER_USAGE_TRANSFER_SRC_BIT
    sharing = SHARING_MODE_EXCLUSIVE
    vkb = Vulkan.Buffer(Vulkan.Device(m),
                        sizeof(m),
                        usage,
                        sharing,
                        map(vkindex, QueueFamily[]);
                        next=info,
                       )
    b = Buffer(Device(m), vkb, sizeof(m), usage, sharing, Ref(m), [])
    bind!(m, b)
    b
end
cuda_buffer(dev::Device, v::ExportableCuArray) = cuda_buffer(Memory(dev, v))

function _cuda_make_framebuffers(ctx::VulkanContext, (x, y), rndr_pass::RenderPass, vs::AbstractVector{ImageView})
    map(vs) do v
        create_framebuffer(ctx.device.device, rndr_pass, [v], x, y, 1) |> unwrap
    end
end


mutable struct CuDisplay
    context::VulkanContext

    window::Window
    surface::Surface

    swapchain::SwapchainKHR

    cuda_array::ExportableCuArray
    cuda_array_buffer::Buffer

    nframes::Int

    t0::Float64  # timing

    frame_resized::Atomic{Bool}

    command_buffers::Vector{CommandBuffer}

    image_views::Vector{ImageView}

    framebuffers::Vector{Framebuffer}

    render_pass::RenderPass

    image_available::Vector{Semaphore}
    render_finished::Vector{Semaphore}
    in_flight::Vector{Fence}
    
    #TODO: need a finalizer that frees the command buffers
end

function _cuda_swapchain(ctx::VulkanContext, surface::Surface;
                         format::Format=SWAPCHAIN_IMAGE_FORMAT,
                         usage::ImageUsageFlag=IMAGE_USAGE_COLOR_ATTACHMENT_BIT ∪ IMAGE_USAGE_TRANSFER_DST_BIT,
                        )
    ext = currentextent(surface)
    o = create_swapchain_khr(Vulkan.Device(ctx), SurfaceKHR(surface),
                             UInt32(minimagecount(surface)),
                             format,
                             COLOR_SPACE_SRGB_NONLINEAR_KHR,  # color space
                             ext,  # window size, in this case
                             UInt32(1),  # image array layers... don't know what that means
                             usage,
                             SHARING_MODE_EXCLUSIVE,  # not too sure what this means
                             [],  # queue family indices
                             currenttransform(surface),
                             COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                             defaultpresentmode(surface),
                             true,  # means we don't care about pixels that are not drawn
                            ) |> unwrap
    @debug("swapchain created", width=Int(ext.width), height=Int(ext.height))
    o
end

function _cuda_window(title::AbstractString, Δx::Integer, Δy::Integer; resizable::Bool=false)
    frame_resized = Atomic{Bool}(false)
    cb = (u, x, y) -> (frame_resized[] = true)
    w = Window(title, Δx, Δy; resizable, framebuffer_size_callback=cb)
    (frame_resized, w)
end

function _cuda_sync_objects(ctx::VulkanContext, n::Integer)
    img_avail = map(j -> Semaphore(ctx.device), 1:n)
    rndr_fin = map(j -> Semaphore(ctx.device), 1:n)
    in_flight = map(j -> Fence(ctx.device; flags=FENCE_CREATE_SIGNALED_BIT), 1:n)
    (img_avail, rndr_fin, in_flight)
end

function _cuda_render_pass(ctx::VulkanContext, format=SWAPCHAIN_IMAGE_FORMAT)
    a = [AttachmentDescription(format,
                               SAMPLE_COUNT_1_BIT,
                               ATTACHMENT_LOAD_OP_CLEAR,
                               ATTACHMENT_STORE_OP_STORE,
                               ATTACHMENT_LOAD_OP_DONT_CARE,
                               ATTACHMENT_STORE_OP_DONT_CARE,
                               IMAGE_LAYOUT_UNDEFINED,
                               IMAGE_LAYOUT_PRESENT_SRC_KHR
                              )
        ]
    sp = [SubpassDescription(PIPELINE_BIND_POINT_GRAPHICS,
                             [], # input attachments
                             [AttachmentReference(0, IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)], # color attachments
                             [], # preserve attachments
                            )
         ]
    create_render_pass(ctx.device.device, a, sp, []) |> unwrap
end

function CuDisplay(ctx::VulkanContext, init_img::AbstractMatrix{<:Colorant};
                   window_title::AbstractString="cuda_display",
                   resizable::Bool=false,
                   nframes::Integer=2,
                  )
    (Δx, Δy) = size(init_img)
    @debug("initializing CuDisplay", Δx, Δy)
    GLFW.Init()  # TODO: is this really appropriate here?

    frame_resized = Atomic{Bool}(false)
    (frame_resized, w) = _cuda_window(window_title, Δx, Δy; resizable)
    surface = Surface(ctx, w)

    swc = _cuda_swapchain(ctx, surface)

    a = ExportableCuArray{BGRA{N0f8},2}(undef, (Δx, Δy))
    a.array .= init_img
    b = cuda_buffer(ctx.device, a)

    (img_avail, rndr_fin, in_flight) = _cuda_sync_objects(ctx, nframes)

    info = CommandBufferAllocateInfo(ctx.cmdpool_gfx, COMMAND_BUFFER_LEVEL_PRIMARY, nframes)
    cmd_bufs = allocate_command_buffers(ctx.device.device, info) |> unwrap

    rndr_pass = _cuda_render_pass(ctx)

    imgs = get_swapchain_images_khr(ctx.device.device, swc) |> unwrap
    img_views = vkImageView.(imgs, (SWAPCHAIN_IMAGE_FORMAT,))

    framebufs = _cuda_make_framebuffers(ctx, (Δx, Δy), rndr_pass, img_views)

    CuDisplay(ctx,
              w, surface,
              swc,
              a, b,
              nframes,
              0.0,
              frame_resized,
              cmd_bufs,
              img_views,
              framebufs,
              rndr_pass,
              img_avail, rndr_fin, in_flight,
             )
end
CuDisplay(init_img; kw...) = CuDisplay(vkglobal(), init_img; kw...)

getextent(e::CuDisplay) = currentextent(e.surface)
getextent!(e::CuDisplay) = currentextent(e.surface)

function get_swapchain_images(e::CuDisplay)
    ext = getextent(e)
    imgs = get_swapchain_images_khr(e.context.device.device, e.swapchain) |> unwrap
    map(imgs) do img
        Image{BGRA{N0f8}}(e.context.device, img, (ext.width, ext.height), RefValue{Memory}(),
                          IMAGE_USAGE_COLOR_ATTACHMENT_BIT ∪ IMAGE_USAGE_TRANSFER_DST_BIT,
                          IMAGE_TYPE_2D, Ref(IMAGE_LAYOUT_PRESENT_SRC_KHR),
                          IMAGE_TILING_OPTIMAL, [],
                         )
    end
end


function record_cuda_render_command!(e::CuDisplay, buf::CommandBuffer, idx::Integer)
    ext = getextent(e)

    begin_command_buffer(buf, CommandBufferBeginInfo()) |> unwrap

    clear_color = Float32.((0, 0, 0, 1)) |> ClearColorValue |> ClearValue  # this should be black
    rpinfo = RenderPassBeginInfo(e.render_pass, e.framebuffers[idx], Rect2D(Offset2D(0, 0), getextent(e)), [clear_color])

    cmd_begin_render_pass(buf, rpinfo, SUBPASS_CONTENTS_INLINE)

    cmd_set_viewport(buf, [Viewport(0, 0, ext.width, ext.height, 0, 1)])

    cmd_set_scissor(buf, [Rect2D(Offset2D(0, 0), getextent(e))])

    cmd_end_render_pass(buf)

    end_command_buffer(buf) |> unwrap
end

function drawframe!(e::CuDisplay, idx::Integer=1; timeout_draw::Integer=typemax(Int))
    wait(e.context.device, e.in_flight[idx]; timeout=timeout_draw)

    res = acquire_next_image_khr(e.context.device.device, e.swapchain, UInt(timeout_draw);
                                 semaphore=e.image_available[idx],
                                )

    if iserror(res)
        err = unwrap_error(res)
        if err.code ∈ (ERROR_OUT_OF_DATE_KHR, SUBOPTIMAL_KHR)
            @debug("acquire next image returned non-zero", res)
            remake_swapchain!(e)
            return e
        end
    else
        (img, res) = unwrap(res)
    end

    reset(e.context.device, e.in_flight[idx])

    cbuf = e.command_buffers[idx]
    reset_command_buffer(cbuf) |> unwrap
    record_cuda_render_command!(e, cbuf, img+1) |> unwrap

    info = SubmitInfo([e.image_available[idx]], [PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT], [cbuf],
                      [e.render_finished[idx]]
                     )

    queue_submit(Queue(gfxqueuefamily(e.context.device)), [info]; fence=e.in_flight[idx]) |> unwrap

    imgs = get_swapchain_images(e)
    for dst ∈ imgs
        copyto!(dst, e.cuda_array_buffer)
    end

    pinfo = PresentInfoKHR([e.render_finished[idx]], [e.swapchain], [img])

    res = queue_present_khr(Queue(presentqueuefamily(e.surface)), pinfo)

    if iserror(res)
        err = unwrap_error(res)
        if err.code ∈ (ERROR_OUT_OF_DATE_KHR, SUBOPTIMAL_KHR) || e.frame_resized
            @debug("queue_present_khr returned non-zero or frame must be resized", res)
            e.frame_resized = false
            remake_swapchain!(e)
        else
            throw(err)
        end
    end

    mod1(idx+1, e.nframes)
end

#TODO: sometimes when terminating we get a warning that smaphore is destroyed before command buffer


function run!(e::CuDisplay)
    @debug("cuda main engine loop starting")
    idx = 1
    while !shouldclose(e.surface)
        try
            GLFW.PollEvents()
            idx = drawframe!(e, idx)
        catch err
            @error("encountered error during engine execution loop")
            destroy!(e.surface)
            rethrow(err)
        end
    end
    @debug("main engine loop terminated")
    destroy!(e.surface)
    GLFW.Terminate()
    nothing
end


