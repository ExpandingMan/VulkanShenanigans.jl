
const DEFAULT_IMAGE_USAGE = IMAGE_USAGE_TRANSFER_SRC_BIT ∪ IMAGE_USAGE_TRANSFER_DST_BIT ∪ IMAGE_USAGE_SAMPLED_BIT
const DEFAULT_IMAGE_MEM_PROPS = MEMORY_PROPERTY_DEVICE_LOCAL_BIT

#TODO: some of these fields might need options to be "unknown" becuase we don't have a lot of
#control if we get the images from the swapchain or something

struct Image{T<:Colorant}
    device::Device
    image::Vulkan.Image
    size::Tuple{Int,Int}
    memory::RefValue{Memory}
    usage::ImageUsageFlag
    type::ImageType
    layout::RefValue{ImageLayout}  # WARN: not thread safe
    tiling::ImageTiling
    queue_families::Vector{QueueFamily}
end

Device(img::Image) = img.device
Vulkan.Device(img::Image) = Vulkan.Device(Device(img))
Vulkan.Image(img::Image) = img.image
Vulkan.Format(img::Image{T}) where {T} = Vulkan.Format(T)
Vulkan.ImageLayout(img::Image) = img.layout[]

# this is private to image, only gets changed when running command buffers
function _setlayout!(img::Image, lyt::ImageLayout)
    img.layout[] = lyt
end

hasmemory(img::Image) = isassigned(img.memory)

function Memory(img::Image)
    hasmemory(img) || error("image memory is not bound or unknown")
    img.memory[]
end

Vulkan.DeviceMemory(img::Image) = Vulkan.DeviceMemory(Memory(img))


# remember these are *neverh visible to host
function Image{T}(dev::Device, ::typeof(undef), sz::Tuple{<:Integer,<:Integer},
                  usage::ImageUsageFlag=DEFAULT_IMAGE_USAGE;
                  allocate::Bool=true,  # don't even know if false should be valid
                  mip_levels::Integer=1,
                  array_layers::Integer=1,  # what is this exactly?
                  samples::SampleCountFlag=SAMPLE_COUNT_1_BIT,
                  type::ImageType=IMAGE_TYPE_2D,
                  tiling::ImageTiling=IMAGE_TILING_OPTIMAL,
                  sharing_mode::SharingMode=SHARING_MODE_EXCLUSIVE,
                  layout::ImageLayout=IMAGE_LAYOUT_PREINITIALIZED,
                  memory_properties::MemoryPropertyFlag=DEFAULT_IMAGE_MEM_PROPS,
                  queue_families::AbstractVector{<:QueueFamily}=QueueFamily[],
                 ) where {T}
    idx = map(vkindex, queue_families)
    img = Vulkan.Image(Vulkan.Device(dev), type, Vulkan.Format(T), Extent3D(sz..., 1), mip_levels,
                       array_layers, samples, tiling, usage, sharing_mode,
                       idx, layout,
                      )
    o = Image{T}(dev, img, sz, RefValue{Memory}(), usage, type, Ref(layout), tiling, queue_families)
    allocate && allocate!(Memory, o, memory_properties)
    o
end
function Image{T}(::typeof(undef), sz::Tuple{<:Integer,<:Integer}; kw...) where {T}
    Image{T}(vkglobal(Device), undef, sz; kw...)
end

function MemoryRequest(img::Image, props::MemoryPropertyFlag=DEFAULT_IMAGE_MEM_PROPS)
    MemoryRequest(get_image_memory_requirements(Vulkan.Device(img), Vulkan.Image(img)), props)
end

function allocate!(::Type{Memory}, img::Image, props::MemoryPropertyFlag=DEFAULT_IMAGE_MEM_PROPS)
    img.memory[] = Memory(img, undef, props)
end

ColorTypes.Colorant(img::Image{T}) where {T} = T

Base.size(img::Image) = img.size

Vulkan.Extent2D(img::Image) = Extent2D(img.size...)
Vulkan.Extent3D(img::Image) = Extent3D(img.size..., 1)

default_image_subresource_layers() = ImageSubresourceLayers(IMAGE_ASPECT_COLOR_BIT, 0, 0, 1)
default_image_subresource_range() = ImageSubresourceRange(IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1)

function bind!(m::Memory, img::Image; offset::Integer=0)
    bind_image_memory(Vulkan.Device(m), Vulkan.Image(img), Vulkan.DeviceMemory(m), offset) |> unwrap
    m
end

function Memory(img::Image, ::typeof(undef), props::MemoryPropertyFlag=DEFAULT_IMAGE_MEM_PROPS; kw...)
    m = Memory(Device(img), undef, MemoryRequest(img, props))
    bind!(m, img; kw...)
end

function _transition_layout_access(l::ImageLayout)
    if l == IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        ACCESS_TRANSFER_WRITE_BIT
    elseif l == IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        ACCESS_TRANSFER_READ_BIT
    elseif l == IMAGE_LAYOUT_GENERAL
        ACCESS_TRANSFER_READ_BIT ∪ ACCESS_TRANSFER_WRITE_BIT
    elseif l == IMAGE_LAYOUT_PRESENT_SRC_KHR
        ACCESS_NONE
    elseif l == IMAGE_LAYOUT_PREINITIALIZED  # this one's dubious
        ACCESS_NONE
    elseif l == IMAGE_LAYOUT_UNDEFINED
        ACCESS_NONE
    else
        error("unable to infer access flags for image layout $l")
    end
end

function _transition_layout_pipeline_stage(l::ImageLayout)
    if l ∈ (IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, IMAGE_LAYOUT_GENERAL)
        PIPELINE_STAGE_TRANSFER_BIT
    elseif l == IMAGE_LAYOUT_PRESENT_SRC_KHR  # again very dubious
        PIPELINE_STAGE_ALL_GRAPHICS_BIT
    elseif l ∈ (IMAGE_LAYOUT_UNDEFINED, IMAGE_LAYOUT_PREINITIALIZED)
        PIPELINE_STAGE_TOP_OF_PIPE_BIT
    else
        error("unable to infer pipeline stage flags for image layout $l")
    end
end

function cmd_transitionlayout(bcmd::CommandBuffer, img::Image, lyt1::ImageLayout, lyt2::ImageLayout;
                              src_queue_family_index::Integer=QUEUE_FAMILY_IGNORED,
                              dst_queue_family_index::Integer=QUEUE_FAMILY_IGNORED,
                              subresource_range::ImageSubresourceRange=default_image_subresource_range(),
                             )
    (srcaccess, dstaccess) = _transition_layout_access.((lyt1, lyt2))
    (srcpstage, dstpstage) = _transition_layout_pipeline_stage.((lyt1, lyt2))
    # vulkan seems to always permit the source layout to be undefined
    # seems like the safest option and don't see a reason not to do it
    bar = ImageMemoryBarrier(srcaccess, dstaccess, IMAGE_LAYOUT_UNDEFINED, lyt2,
                             src_queue_family_index, dst_queue_family_index,
                             Vulkan.Image(img), subresource_range,
                            )
    cmd_pipeline_barrier(bcmd, [], [], [bar]; src_stage_mask=srcpstage, dst_stage_mask=dstpstage)
    nothing
end

function cmd_copyto_notransition(bcmd::CommandBuffer, img::Image, b::Buffer;
                                 subresource_layers::ImageSubresourceLayers=default_image_subresource_layers(),
                                 layout::ImageLayout=IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                 buffer_offset::Integer=0,
                                 offset::Offset3D=Offset3D(0,0,0),
                                )
    #TODO: why are buffer row length and image height always 0??
    bcopy = BufferImageCopy(buffer_offset, 0, 0, subresource_layers, offset, Vulkan.Extent3D(img))
    cmd_copy_buffer_to_image(bcmd, Vulkan.Buffer(b), Vulkan.Image(img), layout, [bcopy])
    nothing
end

function cmd_copyto_notransition(bcmd::CommandBuffer, b::Buffer, img::Image;
                                 subresource_layers::ImageSubresourceLayers=default_image_subresource_layers(),
                                 buffer_offset::Integer=0,
                                 offset::Offset3D=Offset3D(0,0,0),
                                )
    bcopy = BufferImageCopy(buffer_offset, 0, 0, subresource_layers, offset, Vulkan.Extent3D(img))
    cmd_copy_image_to_buffer(bcmd, Vulkan.Image(img), Vulkan.ImageLayout(img), Vulkan.Buffer(b), [bcopy])
end

function Base.copyto!(ctx::VulkanContext, img::Image, b::Buffer;
                      preserve_layout::Bool=true,
                      queue::Queue=gfxqueue(ctx),
                      kw...
                     )
    cmdp = gfxcmdpool(ctx)
    commandexe(cmdp, queue; kw...) do bcmd
        (lyt1, lyt2) = (ImageLayout(img), IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
        cmd_transitionlayout(bcmd, img, lyt1, lyt2)
        _setlayout!(img, lyt2)
        cmd_copyto_notransition(bcmd, img, b)
        if preserve_layout && (lyt1 ∉ (IMAGE_LAYOUT_UNDEFINED, IMAGE_LAYOUT_PREINITIALIZED))
            cmd_transitionlayout(bcmd, img, lyt2, lyt1)
            _setlayout!(img, lyt1)
        end
    end
    img
end
Base.copyto!(img::Image, b::Buffer; kw...) = copyto!(vkglobal(), img, b; kw...)

function Base.copyto!(ctx::VulkanContext, b::Buffer, img::Image;
                      preserve_layout::Bool=true,
                      queue::Queue=gfxqueue(ctx),
                      kw...
                     )
    cmdp = gfxcmdpool(ctx)
    commandexe(cmdp, queue; kw...) do bcmd
        (lyt1, lyt2) = (ImageLayout(img), IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
        cmd_transitionlayout(bcmd, img, lyt1, lyt2)
        _setlayout!(img, lyt2)
        cmd_copyto_notransition(bcmd, b, img)
        # if this was the case we don't transition again since we can't transition to undefined
        if preserve_layout && (lyt1 ∉ (IMAGE_LAYOUT_UNDEFINED, IMAGE_LAYOUT_PREINITIALIZED))
            cmd_transitionlayout(bcmd, img, lyt2, lyt1)
            _setlayout!(img, lyt1)
        end
    end
    b
end
Base.copyto!(b::Buffer, img::Image; kw...) = copyto!(vkglobal(), b, img; kw...)

Base.copyto!(ctx::VulkanContext, b::BufferArray, img::Image; kw...) = (copyto!(ctx, Buffer(b), img; kw...); b)
Base.copyto!(b::BufferArray, img::Image; kw...) = copyto!(vkglobal(), b, img; kw...)

Base.copyto!(ctx::VulkanContext, img::Image, b::BufferArray; kw...) = copyto!(ctx, img, Buffer(b); kw...)
Base.copyto!(img::Image, b::BufferArray; kw...) = copyto!(vkglobal(), img, b; kw...)

function Base.copyto!(ctx::VulkanContext, img::Image, x::AbstractMatrix{<:Colorant}; kw...)
    copyto!(ctx, img, BufferArray(x); kw...)
end
Base.copyto!(img::Image, x::AbstractMatrix{<:Colorant}; kw...) = copyto!(vkglobal(), img, x; kw...)

function Image(ctx::VulkanContext, x::AbstractMatrix{<:Colorant}, usage::ImageUsageFlag=DEFAULT_IMAGE_USAGE;
               queue::Queue=gfxqueue(ctx),
               kw...
              )
    img = Image{eltype(x)}(Device(ctx), undef, size(x), usage; kw...)
    copyto!(ctx, img, x)
end
function Image(x::AbstractMatrix{<:Colorant}, usage::ImageUsageFlag=DEFAULT_IMAGE_USAGE; kw...)
    Image(vkglobal(), x, usage; kw...)
end

function BufferArray(ctx::VulkanContext, img::Image{T}, usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE; kw...) where {T}
    b = BufferArray{T}(Device(ctx), undef, size(img), usage; kw...)
    copyto!(ctx, b, img)
end
BufferArray(img::Image, usage::BufferUsageFlag=DEFAULT_BUFFER_USAGE; kw...) = BufferArray(vkglobal(), img, usage; kw...)

#TODO: this has to be replaced with something less stupid
# let's try this as a convention for "pirated" constructors
default_component_mapping() = ComponentMapping(ntuple(x -> COMPONENT_SWIZZLE_IDENTITY, 4)...)
function vkImageView(img::Vulkan.Image, format::Format;
                     component_mapping::ComponentMapping=default_component_mapping(),
                     subresource_range::ImageSubresourceRange=default_image_subresource_range(),
                    )
    ImageView(img.device, img, IMAGE_VIEW_TYPE_2D, format, component_mapping, subresource_range)
end

Vulkan.ImageView(img::Image; kw...) = vkImageView(img.image, img.format; kw...)
