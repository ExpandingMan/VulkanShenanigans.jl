#version 450

struct Affine {
    mat3 rot;
    vec3 trans;
};

layout(std140, binding=0) uniform ModelView {
    Affine model;
    Affine view;
} mv;

layout(location=0) in vec3 inPosition;
layout(location=1) in vec3 inColor;
layout(location=2) in vec2 inTexCoord;

layout(location=0) out vec3 fragColor;
layout(location=1) out vec2 fragTexCoord;


vec3 applyaffine(in Affine a, in vec3 x) {
    return a.rot * x + a.trans;
}

// assumes x is in camera coordinates
vec3 perspective(in vec3 x, in float delta) {
    return vec3(x[0]/x[2], x[1]/x[2], 0.0);
}

void main() {
    fragColor = inColor;
    fragTexCoord = inTexCoord;

    vec3 x = applyaffine(mv.model, inPosition);
    vec3 xp = applyaffine(mv.view, x);

    vec3 xpp = perspective(xp, 1.0);

    gl_Position = vec4(xpp, 1.0);
}
