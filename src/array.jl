
const DEFAULT_MEM_PROPS = MEMORY_PROPERTY_HOST_VISIBLE_BIT ∪ MEMORY_PROPERTY_HOST_COHERENT_BIT

# I'm just going to assume that the existing finalizer will already unmap for us...
struct Memory
    device::Device
    memory::DeviceMemory
    size::Int
    typeid::Int
    properties::MemoryPropertyFlag
end

Device(m::Memory) = m.device
Vulkan.Device(m::Memory) = Vulkan.Device(Device(m))
Vulkan.DeviceMemory(m::Memory) = m.memory
Vulkan.MemoryPropertyFlag(m::Memory) = m.properties

Base.sizeof(m::Memory) = m.size

# this occurs very frequently so we define this struct
struct MemoryRequest
    size::Int
    alignment::Int
    allowed_types::Union{Nothing,Vector{Int}}
    properties::MemoryPropertyFlag
end

function MemoryRequest(reqs::MemoryRequirements, props::MemoryPropertyFlag=MemoryPropertyFlag(0))
    m = reqs.memory_type_bits
    ts = (0:(8*sizeof(m)-1)) |> Map(j -> m & (one(typeof(m)) << j) > 0) |> Enumerate(0) |>
        Filter(t -> t[2]) |> Map(t -> t[1]) |> collect
    MemoryRequest(reqs.size, reqs.alignment, ts, props)
end

function MemoryRequest(x::AbstractArray, props::MemoryPropertyFlag=DEFAULT_MEM_PROPS; alignment::Integer=8)
    MemoryRequest(sizeof(x), alignment, nothing, props)
end

function Memory(dev::Device, ::typeof(undef), sz::Integer, typeid::Integer)
    m = allocate_memory(dev.device, sz, typeid) |> unwrap
    Memory(dev, m, sz, typeid, Vulkan.MemoryPropertyFlag(dev, typeid))
end

findfirstmemtype(dev::Device, req::MemoryRequest) = findfirstmemtype(dev, req.properties, req.allowed_types)
findfirstmemtype(req::MemoryRequest) = findfirstmemtype(vkglobal(Device), req)

Memory(dev::Device, ::typeof(undef), req::MemoryRequest) = Memory(dev, undef, req.size, findfirstmemtype(req))    

# this should be considered private because you should only do it with VkArray
function _mapmem(::Type{T}, m::Memory, off::Integer=0, dims=(m.size,)) where {T}
    ptr = map_memory(Device(m).device, m.memory, off, m.size) |> unwrap
    unsafe_wrap(Array, convert(Ptr{T}, ptr), dims)
end

# this is considered private because it's super unsafe
_unmap!(dev::Device, m::Memory) = unmap_memory(dev.device, m.memory)


abstract type VkAbstractArray{T,N} <: AbstractArray{T,N} end

Base.size(v::VkAbstractArray) = size(v.array)

Base.IndexStyle(::Type{<:VkAbstractArray}) = Base.IndexLinear()

Base.getindex(v::VkAbstractArray, j::Int) = v.array[j]

Base.copyto!(v::VkAbstractArray, x::AbstractArray)  = (copyto!(innerarray(v), x); v)

# this is to get the inner AbstractArray
innerarray(x::AbstractArray) = x


struct VkArray{T,N} <: VkAbstractArray{T,N}
    memory::Memory
    array::Array{T,N}
end

const VkVector{T} = VkArray{T,1}
const VkMatrix{T} = VkArray{T,2}

Memory(v::VkArray) = v.memory
Device(v::VkArray) = Device(Memory(v))

innerarray(v::VkArray) = v.array

function VkArray{T}(m::Memory, ::typeof(undef), dims::NTuple{N,Integer}=(sizeof(m)÷sizeof(T),);
                    offset::Integer=0,
                   ) where {T,N}
    a = _mapmem(T, m, offset, dims)
    VkArray{T,N}(m, a)
end

function VkArray{T}(dev::Device, x::AbstractArray;
                    offset::Integer=0,
                    memory_request::MemoryRequest=MemoryRequest(x)
                   ) where {T}
    if !isbitstype(eltype(x))
        throw(ArgumentError("can only create vulkan arrays from bits types"))
    end
    m = Memory(dev, undef, memory_request)
    o = VkArray{T}(m, undef, size(x); offset)
    copyto!(o, x)
end
VkArray(dev::Device, x::AbstractArray; kw...) = VkArray{eltype(x)}(dev, x; kw...)

VkArray{T}(x::AbstractArray; kw...) where {T} = VkArray{T}(vkglobal(Device), x; kw...)
VkArray(x::AbstractArray; kw...) = VkArray{eltype(x)}(x; kw...)

Base.setindex!(v::VkArray, x, j::Int) = setindex!(v.array, x, j)

