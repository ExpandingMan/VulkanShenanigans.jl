

default_framebuffer_size_callback(e::Engine) = (u, x, y) -> (e.frame_resized = true)

make_device!(e::Engine) = (e.device = Device())

getextent(e::Engine) = currentextent(e.surface)
getextent!(e::Engine) = currentextent!(e.surface)

function make_swapchain(e::Engine;
                        format::Format=SWAPCHAIN_IMAGE_FORMAT,
                        usage::ImageUsageFlag=IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                       )
    ext = getextent(e)
    o = create_swapchain_khr(e.context.device.device, SurfaceKHR(e.surface),
                             UInt32(minimagecount(e.surface)),
                             format,
                             COLOR_SPACE_SRGB_NONLINEAR_KHR,  # color space
                             ext,  # window size, in this case
                             UInt32(1),  # image array layers... don't know what that means
                             usage,
                             SHARING_MODE_EXCLUSIVE,  # not too sure what this means
                             [],  # queue family indices
                             currenttransform(e.surface),
                             COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                             defaultpresentmode(e.surface),
                             true,  # means we don't care about pixels that are not drawn
                            ) |> unwrap
    @debug("swapchain created", width=Int(ext.width), height=Int(ext.height))
    o
end
make_swapchain!(e; kw...) = (e.swapchain = make_swapchain(e; kw...))

"""
    destroy_swapchain!

To create a swapchain for a window that already has one we must explicitly destroy the old one, or else it will
crash because it can't have multiple swapchains.
"""
function destroy_swapchain!(e::Engine)
    e.framebuffers = []
    e.image_views = []
    destroy_swapchain_khr(e.device.device, e.swapchain)
    e.swapchain = nothing
end

"""
    remake_swapchain!

If something happens to the window (e.g. dimensions change) we have to re-create the swapchain.  This will call
`destroy_swapchain!` to explicitly destroy the existing swapchain since Vulkan can't infer that we want to get rid
of the old one.
"""
function remake_swapchain!(e::Engine)
    (x, y) = framebuffersize(e.window)
    while x == 0 || y == 0
        (x, y) = framebuffersize(e.window)
        GLFW.WaitEvents()
    end

    @debug("waiting for device to be idle so we can re-create swapchain")

    device_wait_idle(e.device.device) |> unwrap

    @debug("recreating swapchain")

    destroy_swapchain!(e)

    capabilities!(e.surface)

    make_swapchain!(e)
    make_image_views!(e)
    make_framebuffers!(e)
    e
end

function make_window_surface!(e::Engine, Δx::Integer, Δy::Integer;
                              title::AbstractString="",
                              resizable::Bool=true,
                             )
    @debug("creating window surface")
    e.surface = Surface(e.context,
                        Window(title, Δx, Δy; resizable, framebuffer_size_callback=default_framebuffer_size_callback(e)),
                       )
end

get_swapchain_images(e::Engine) = get_swapchain_images_khr(e.context.device.device, e.swapchain) |> unwrap

function make_image_views!(e::Engine)
    imgs = get_swapchain_images(e)
    e.image_views = vkImageView.(imgs, (SWAPCHAIN_IMAGE_FORMAT,))
end

function make_shader_module(e::Engine, code::AbstractVector{UInt32})
    create_shader_module(e.device.device, 4length(code), code) |> unwrap
end
function make_shader_module(e::Engine, fname::AbstractString; kw...)
    make_shader_module(e, glslcompilefile(fname; kw...))
end

_shadersdir(a...) = joinpath(@__DIR__,"glsl",a...)

function triangle_shader_modules(e::Engine)
    o = [make_shader_module(e, _shadersdir("triangle1.vert")), make_shader_module(e, _shadersdir("triangle1.frag"))]
    @debug("created triangle shader modules")
    o
end

function make_render_pass(e::Engine, format=SWAPCHAIN_IMAGE_FORMAT)
    a = [AttachmentDescription(format,
                               SAMPLE_COUNT_1_BIT,
                               ATTACHMENT_LOAD_OP_CLEAR,
                               ATTACHMENT_STORE_OP_STORE,
                               ATTACHMENT_LOAD_OP_DONT_CARE,
                               ATTACHMENT_STORE_OP_DONT_CARE,
                               IMAGE_LAYOUT_UNDEFINED,
                               IMAGE_LAYOUT_PRESENT_SRC_KHR
                              )
        ]
    sp = [SubpassDescription(PIPELINE_BIND_POINT_GRAPHICS,
                             [], # input attachments
                             [AttachmentReference(0, IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)], # color attachments
                             [], # preserve attachments
                            )
         ]
    create_render_pass(e.context.device.device, a, sp, []) |> unwrap
end
make_render_pass!(e::Engine, format=SWAPCHAIN_IMAGE_FORMAT) = (e.render_pass = make_render_pass(e, format))

function _default_color_write_mask()
    |(COLOR_COMPONENT_R_BIT, COLOR_COMPONENT_G_BIT, COLOR_COMPONENT_B_BIT, COLOR_COMPONENT_A_BIT)
end

function viewport_from_extent(e::Engine)
    extent = getextent(e)
    Viewport(0, 0, extent.width, extent.height, 0, 1)
end

function bindingdesc(::Type{T}, idx::Integer) where {T<:Vertex}
    VertexInputBindingDescription(idx, sizeof(T), VERTEX_INPUT_RATE_VERTEX)
end

function inputattrdesc(::Type{T}, idx::Integer) where {T<:Vertex}
    [VertexInputAttributeDescription(0, idx, FORMAT_R32G32_SFLOAT, fieldoffset(T, 1)),
     VertexInputAttributeDescription(1, idx, FORMAT_R32G32B32_SFLOAT, fieldoffset(T, 2)),
     VertexInputAttributeDescription(2, idx, FORMAT_R32G32_SFLOAT, fieldoffset(T, 3)),
    ]
end

function make_graphics_pipeline(e::Engine)
    vp = viewport_from_extent(e)
    sc = Rect2D(Offset2D(0, 0), getextent(e))

    bds = [bindingdesc(Vertex, 0)]
    att = inputattrdesc(Vertex, 0)

    vtx = PipelineVertexInputStateCreateInfo(bds, att)
    ina = PipelineInputAssemblyStateCreateInfo(PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, false)
    vps = PipelineViewportStateCreateInfo(viewports=[vp], scissors=[sc])
    #WARN: might have to mess with culling to see your geometry for now
    rst = PipelineRasterizationStateCreateInfo(false,  # rasterizer discard enable
                                               false,
                                               POLYGON_MODE_FILL,
                                               FRONT_FACE_COUNTER_CLOCKWISE,
                                               false,  # depth bias enable
                                               0.0f0,  # depth bias constant factor
                                               0.0f0,  # depth bias clamp
                                               0.0f0,  # depth bias slope factor
                                               1.0f0;  # line width
                                               # restore this to draw only one side of texture
                                               #cull_mode=CULL_MODE_BACK_BIT,
                                              )
    msm = PipelineMultisampleStateCreateInfo(SAMPLE_COUNT_1_BIT, false, 1.0f0, false, false)

    cba = PipelineColorBlendAttachmentState(false, BLEND_FACTOR_ONE, BLEND_FACTOR_ZERO, BLEND_OP_ADD,
                                            BLEND_FACTOR_ONE, BLEND_FACTOR_ZERO, BLEND_OP_ADD,
                                            _default_color_write_mask()
                                           )
    cb = PipelineColorBlendStateCreateInfo(false, LOGIC_OP_COPY, [cba], ntuple(i -> 0.0f0, 4))

    sts = PipelineDynamicStateCreateInfo([DYNAMIC_STATE_VIEWPORT, DYNAMIC_STATE_SCISSOR])

    pl = PipelineLayout(e.device.device, [e.descriptor_set_layout], [])

    shader_modules = triangle_shader_modules(e)
    shader_stages = [PipelineShaderStageCreateInfo(SHADER_STAGE_VERTEX_BIT, shader_modules[1], "main"),
                     PipelineShaderStageCreateInfo(SHADER_STAGE_FRAGMENT_BIT, shader_modules[2], "main"),
                    ]
    pipeinfo = GraphicsPipelineCreateInfo(shader_stages,
                                          rst,
                                          pl,
                                          0,  # subpass
                                          -1;  # base pipeline index (none exists)
                                          vertex_input_state=vtx,
                                          input_assembly_state=ina,
                                          viewport_state=vps,
                                          multisample_state=msm,
                                          color_blend_state=cb,
                                          dynamic_state=sts,
                                          render_pass=e.render_pass,
                                         )

    (pipes, result) = create_graphics_pipelines(e.device.device, [pipeinfo]) |> unwrap
    @info("created graphics pipeline", result)
    (pipes, pl)
end
function make_graphics_pipeline!(e::Engine)
    (e.pipelines, e.pipeline_layout) = make_graphics_pipeline(e)
end

function make_framebuffers(e::Engine)
    ext = getextent(e)
    map(e.image_views) do v
        create_framebuffer(e.context.device.device, e.render_pass, [v], ext.width, ext.height, 1) |> unwrap
    end
end
make_framebuffers!(e::Engine) = (e.framebuffers = make_framebuffers(e))

function make_commandbuffers(e::Engine)
    info = CommandBufferAllocateInfo(e.context.cmdpool_gfx, COMMAND_BUFFER_LEVEL_PRIMARY, e.max_frames_in_flight)
    allocate_command_buffers(e.context.device.device, info) |> unwrap
end
make_commandbuffers!(e::Engine) = (e.command_buffers = make_commandbuffers(e))

#TODO: lots of this will have to change to depend on geometry to draw
# this will be use by drawframe!
function record_render_command!(e::Engine, buf::CommandBuffer, idx::Integer)
    begin_command_buffer(buf, CommandBufferBeginInfo()) |> unwrap

    clear_color = Float32.((0, 0, 0, 1)) |> ClearColorValue |> ClearValue  # this should be black
    rpinfo = RenderPassBeginInfo(e.render_pass, e.framebuffers[idx], Rect2D(Offset2D(0, 0), getextent(e)), [clear_color])

    cmd_begin_render_pass(buf, rpinfo, SUBPASS_CONTENTS_INLINE)

    #TODO: right now there is only one pipeline right? do we do this to all of them? if not which?
    cmd_bind_pipeline(buf, PIPELINE_BIND_POINT_GRAPHICS, first(e.pipelines))

    cmd_set_viewport(buf, [viewport_from_extent(e)])

    cmd_set_scissor(buf, [Rect2D(Offset2D(0, 0), getextent(e))])

    cmd_bind_vertex_buffers(buf, [e.vtx_buffer.buffer], [0])

    cmd_bind_index_buffer(buf, e.idx_buffer.buffer, 0, INDEX_TYPE_UINT32)

    cmd_bind_descriptor_sets(buf, PIPELINE_BIND_POINT_GRAPHICS, e.pipeline_layout, 0, [e.descriptor_sets[idx]], [])

    #TODO: this needs to see geometry so it knows what arguments to have
    cmd_draw_indexed(buf, 6, 1, 0, 0, 0)

    cmd_end_render_pass(buf)

    end_command_buffer(buf) |> unwrap
end
function record_render_command!(e::Engine, cmd_idx::Integer=1, idx::Integer=1)
    record_render_command!(e, e.command_buffers[cmd_idx], idx)
end

function make_sync_objects!(e::Engine)
    e.image_available = map(i -> Semaphore(e.context.device), 1:e.max_frames_in_flight)
    e.render_finished = map(i -> Semaphore(e.context.device), 1:e.max_frames_in_flight)
    e.in_flight = map(i -> Fence(e.context.device; flags=FENCE_CREATE_SIGNALED_BIT), 1:e.max_frames_in_flight)
end

function stagedtobuffer(e::Engine, x, usage::BufferUsageFlag, q::Queue=Queue(graphicsqueuefamily(e.device)); kw...)
    stagedtobuffer(e.device, x, usage, e.context.cmdpool_gfx, q; kw...)
end

function make_vtx_buffer!(e::Engine, vtxs::AbstractVector)
    e.vtx_buffer = stagedtobuffer(e, vtxs, BUFFER_USAGE_VERTEX_BUFFER_BIT)
end

function make_idx_buffer!(e::Engine, idxs::AbstractVector{Int32})
    e.idx_buffer = stagedtobuffer(e, idxs, BUFFER_USAGE_INDEX_BUFFER_BIT)
end

function make_descriptor_set_layout(e::Engine)
    bdg1 = DescriptorSetLayoutBinding(0, DESCRIPTOR_TYPE_UNIFORM_BUFFER, SHADER_STAGE_VERTEX_BIT; descriptor_count=1)
    bdg2 = DescriptorSetLayoutBinding(1, DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, SHADER_STAGE_FRAGMENT_BIT;
                                      descriptor_count=1)
    DescriptorSetLayout(e.device.device, [bdg1, bdg2])
end
make_descriptor_set_layout!(e::Engine) = (e.descriptor_set_layout = make_descriptor_set_layout(e))

function make_uniform_buffers_raw(e::Engine, x)
    bufs = map(i -> tobuffer(e.device, x, BUFFER_USAGE_UNIFORM_BUFFER_BIT), 1:e.max_frames_in_flight)
end
make_uniform_buffers_raw!(e::Engine, x) = (e.uniform_buffers = make_uniform_buffers_raw(e, x))

function make_descriptor_pool(e::Engine)
    ps1 = DescriptorPoolSize(DESCRIPTOR_TYPE_UNIFORM_BUFFER, e.max_frames_in_flight)
    ps2 = DescriptorPoolSize(DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, e.max_frames_in_flight)
    DescriptorPool(e.device.device, e.max_frames_in_flight, [ps1, ps2])
end
make_descriptor_pool!(e::Engine) = (e.descriptor_pool = make_descriptor_pool(e))

function make_descriptor_sets(e::Engine, x::AbstractVector)
    info = DescriptorSetAllocateInfo(e.descriptor_pool, [e.descriptor_set_layout for i ∈ 1:e.max_frames_in_flight])
    ds = allocate_descriptor_sets(e.device.device, info) |> unwrap

    for i ∈ 1:e.max_frames_in_flight
        # you can *NOT* use sizeof(buffer) here, it might be padded
        # out the right way to get size
        buf = e.uniform_buffers[i]
        bufinfo = DescriptorBufferInfo(buf.buffer, 0, length(x))
        imginfo = DescriptorImageInfo(e.texture_sampler, e.texture_image_view, IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
        ws1 = WriteDescriptorSet(ds[i], 0, 0, DESCRIPTOR_TYPE_UNIFORM_BUFFER, [], [bufinfo], [])
        ws2 = WriteDescriptorSet(ds[i], 1, 0, DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, [imginfo], [], [])
        update_descriptor_sets(e.device.device, [ws1, ws2], [])
    end

    ds
end
make_descriptor_sets!(e::Engine, x::AbstractVector) = (e.descriptor_sets = make_descriptor_sets(e, x))

function insertshaderbuffers!(𝒻, e::Engine)
    x = 𝒻(e, worldtime() - e.t0)
    make_uniform_buffers_raw!(e, x)
    make_descriptor_pool!(e)
    make_descriptor_sets!(e, x)
    e
end

# we'll take image as an arg for now
set_texture_image!(e::Engine, img::Image) = (e.texture_image = img)

make_texture_image_view!(e::Engine) = (e.texture_image_view = ImageView(e.texture_image))

#TODO: lots of experiments here while I figure out image serialization
function make_texture_sampler(e::Engine)
    filter = FILTER_LINEAR
    addrmode = SAMPLER_ADDRESS_MODE_REPEAT
    Sampler(e.device.device, filter, filter, SAMPLER_MIPMAP_MODE_LINEAR,
            addrmode, addrmode, addrmode,
            0.0,  # mip_lod_bias
            false,  # anisotropy enable
            1.0,  # max anisotropy, its weird that this is a float, isn't it?
            true,  # compare enable (normalized texture coordinates)
            COMPARE_OP_ALWAYS,
            0.0, 0.0,  # min, max lod
            BORDER_COLOR_INT_OPAQUE_BLACK,
            false,  # unnormalized coordiantes
           )
end
make_texture_sampler!(e::Engine) = (e.texture_sampler = make_texture_sampler(e))

#FUCK: everything runs without error but textures have weird artiracts

function default_texture(e)
    #x = FileIO.load(joinpath(@__DIR__,"..","data","textures","pcb1.jpg"))

    sz = (128, 128)
    x1 = fill(RGB(1,0,0), sz)
    x2 = fill(RGB(0,1,0), sz)
    x3 = fill(RGB(0,0,1), sz)
    x4 = fill(RGB(0,1,1), sz)

    x = [x1 x2
         x3 x4]

    x = RGBA.(x)
    @info("loading texture")
    Image(e.device, x)
end

# right now this is a rhombus with edges hitting the edge of the screen to demonstrate coordinates
default_vertices() = [Vertex([-0.5, -0.5, 0.0], [1.0, 0.0, 0.0], [1.0, 0.0]),
                      Vertex([0.5, -0.5, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0]),
                      Vertex([0.5, 0.5, 0.0], [0.0, 0.0, 1.0], [0.0, 1.0]),
                      Vertex([-0.5, 0.5, 0.0], [1.0, 1.0, 1.0], [1.0, 1.0]),
                     ]

default_vertex_indices() = Int32[0, 1, 2, 2, 3, 0]

#TODO: obviously this is going to have to be replaced
function framecall(ϵ, t::Real)
    ω₁ = 2π/4.0 
    ω₂ = 2π/4.0
    model = Affine(RotY(ω₁*t), [0, 0, -3.0])
    view = one(Affine)
    proj = one(Affine)
    std140([model, view, proj])
end

function init!(e::Engine, Δx::Integer=800, Δy::Integer=600;
               validate::Bool=true,
               window_title::AbstractString="vulkan_tutorial",
              )
    @debug("initializing engine", Δx, Δy, window_title)
    GLFW.Init()
    make_device!(e)
    make_window_surface!(e, Δx, Δy; title=window_title)
    make_swapchain!(e)
    make_image_views!(e)
    make_render_pass!(e)
    make_descriptor_set_layout!(e)
    make_graphics_pipeline!(e)
    make_framebuffers!(e)
    make_commandpool!(e)
    set_texture_image!(e, default_texture(e))
    make_texture_image_view!(e)
    make_texture_sampler!(e)
    make_vtx_buffer!(e, default_vertices())
    make_idx_buffer!(e, default_vertex_indices())
    make_commandbuffers!(e)
    make_sync_objects!(e)

    # set start time... probably makes sense to update each frame and compute δt
    e.t0 = worldtime()

    e
end

function update_uniform_buffers!(𝒻, e::Engine, idx::Integer)
    o = 𝒻(e, worldtime() - e.t0)
    isnothing(o) && return nothing
    copyto!(e.uniform_buffers[idx], o)
end

function drawframe!(𝒻, e::Engine;
                    timeout_draw::Integer=e.config_timeout_draw,
                   )
    idx = e.frame_idx  # convenience

    wait(e.device, e.in_flight[idx]; timeout=timeout_draw)

    res = acquire_next_image_khr(e.device.device, e.swapchain, UInt(timeout_draw); semaphore=e.image_available[idx])

    if iserror(res)
        err = unwrap_error(res)
        if err.code ∈ (ERROR_OUT_OF_DATE_KHR, SUBOPTIMAL_KHR)
            @debug("acquire next image returned non-zero", res)
            remake_swapchain!(e)
            return e
        end
    else
        (img, res) = unwrap(res)
    end

    update_uniform_buffers!(𝒻, e, idx)

    reset(e.device, e.in_flight[idx])

    cbuf = e.command_buffers[idx]
    reset_command_buffer(cbuf) |> unwrap
    record_render_command!(e, cbuf, img+1) |> unwrap

    info = SubmitInfo([e.image_available[idx]], [PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT], [cbuf],
                      [e.render_finished[idx]]
                     )

    queue_submit(Queue(graphicsqueuefamily(e.device)), [info]; fence=e.in_flight[idx]) |> unwrap

    pinfo = PresentInfoKHR([e.render_finished[idx]], [e.swapchain], [img])

    res = queue_present_khr(Queue(presentqueuefamily(e.surface)), pinfo)

    if iserror(res)
        err = unwrap_error(res)
        if err.code ∈ (ERROR_OUT_OF_DATE_KHR, SUBOPTIMAL_KHR) || e.frame_resized
            @debug("queue_present_khr returned non-zero or frame must be resized", res)
            e.frame_resized = false
            remake_swapchain!(e)
        else
            throw(err)
        end
    end

    e.frame_idx = mod1(idx+1, e.max_frames_in_flight)

    e
end

function destroy!(e::Engine)
    free_command_buffers(e.context.device.device, e.context.cmdpool_gfx, e.command_buffers)
    @debug("command buffers freed")
    nothing
end


function main(𝒻, e::Engine)
    @debug("main engine loop starting")
    insertshaderbuffers!(𝒻, e)
    while !shouldclose(e.surface)
        try
            GLFW.PollEvents()
            drawframe!(𝒻, e)
        catch err
            @error("encoutered error during engine execution loop")
            destroy!(e.surface)
            rethrow(err)
        end
    end
    @debug("main engine loop terminated")
    # if we reached here it means window should be destroyed
    destroy!(e.surface)
    GLFW.Terminate()
    wait(e.device)
    # still a little confused why command buffers should be destroyed here...
    destroy!(e)
end
main(e::Engine) = main((ϵ, y) -> nothing, e)
