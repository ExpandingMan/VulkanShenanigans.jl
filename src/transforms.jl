
#====================================================================================================
       NOTE

The following implements the std140 layout which is described
[here](https://registry.khronos.org/OpenGL/specs/gl/glspec45.core.pdf#page=159)
and should also match the Vulkan "extended" layout which is described
[here](https://registry.khronos.org/vulkan/specs/1.3-extensions/html/chap15.html#interfaces-resources-layout)

Since I am still rather confused about the layouts, just to be safe you shoud *explicitly include
the std140 annotation in all GLSL structs*.

I believe that you are forced to use this heavily padded format for all `uniform` structs, but
other buffer types may use the packed "scalar" format described in the Vulkan spec.

When SPIRV.jl matures more you will want to swap this out for that.
====================================================================================================#

function writepadto(io::IO, a::Integer)
    n = 0
    p = position(io)
    for i ∈ 1:(a*cld(p, a) - p)
        n += write(io, 0x00)
    end
    n
end

function writealigned(io::IO, a::Integer, x)
    n = writepadto(io, a)
    n += write(io, x)
    n += writepadto(io, a)  # ensure end padding
    n
end

std140alignment(::Type{T}) where {T} = sizeof(T)
std140alignment(::Type{<:AbstractVector{T}}) where {T} = 16cld(std140alignment(T), 16)

std140alignment(x::Real) = std140alignment(typeof(x))
std140alignment(x::AbstractVector) = std140alignment(typeof(x))
std140alignment(x::AbstractMatrix) = maximum(std140alignment, eachcol(x))

std140alignment(x)  = 16cld(maximum(ϕ -> std140alignment(getfield(x, ϕ)), 1:nfields(x)), 16)

std140(io::IO, x::Union{Real,AbstractVector{<:Real}}) = writealigned(io, std140alignment(x), x)
function std140(io::IO, x::AbstractVector)
    a = std140alignment(x)
    n = writepadto(io, a)
    n += sum(ξ -> std140(io, ξ), x)
    n += writepadto(io, a)
end
function std140(io::IO, x::AbstractMatrix)
    a = std140alignment(x)
    n = writepadto(io, a)
    n += sum(c -> std140(io, c), eachcol(x))
    n += writepadto(io, a)
end
function std140(io::IO, x)
    a = std140alignment(x)
    n = writepadto(io, a)
    n += sum(ϕ -> std140(io, getfield(x, ϕ)), 1:nfields(x))
    n += writepadto(io, a)
end

function std140(x)
    io = IOBuffer()
    std140(io, x)
    take!(io)
end


struct Affine
    R::SMatrix{3,3,Float32,9}
    δ::SVector{3,Float32}
end

Base.one(::Type{Affine}) = Affine(@SMatrix(Float32[1 0 0; 0 1 0; 0 0 1]), @SVector(Float32[0, 0, 0]))

